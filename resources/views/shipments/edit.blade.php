@extends('layouts.app')

@section('content')

    @if($userLogged->hasRole('admin') || $userLogged->hasRole('manager'))
    <div class="container" style="padding-bottom:5%">
        <div class="card">
        <div class="card-header" style="display:flex; justify-content:space-between">
            <div style="align-self: center;">Edit Shipment</div>
        </div>

        <div class="card-body">

            <form id="shipment_form" method="POST" action="{{ route('shipment-update',$shipment->id) }}" enctype="multipart/form-data">
                @csrf
            <div class="tabs_container">

                <div class="tab-wrap">

                <!-- active tab on page load gets checked attribute -->
                <input type="radio" id="tab1" name="tabGroup1" value="limited" class="tab">
                <label for="tab1">Shipment Detail</label>

                <input type="radio" id="tab2" name="tabGroup1" value="normal" class="tab">
                <label for="tab2">Document & Images Upload</label>

                <div class="tab__content">

                    <div class="form-group row">
                        <label for="ship_name" class="col-md-4 col-form-label text-md-right">Ship Name</label>

                        <div class="col-md-6">
                            <input id="ship_name" type="text" class="form-control @error('ship_name') is-invalid @enderror" name="ship_name" value="{{ $shipment->ship_name }}" required autofocus>

                            @error('ship_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">

                        <label for="total_weight" class="col-md-4 col-xs-6 col-form-label text-md-right">Total Weight</label>

                        <div class="col-md-3 col-xs-6" style="display:flex;white-space: nowrap">
                            <input id="total_weight" style="border-radius:0.25em 0 0 0.25em;" type="text" class="form-control @error('total_weight') is-invalid @enderror" name="total_weight" value="{{ $shipment->total_weight }}">
                                <span class="input-suffix">MT</span>
                                @error('total_weight')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        @if($userLogged->hasRole('admin'))
                        <label for="total_weight_value" style="display:flex;white-space: nowrap" class="col-md-1 col-xs-6 col-form-label text-md-right">Price /MT</label>

                        <div class="col-md-2 col-xs-6" style="display:flex;white-space: nowrap">
                            <span class="input-prefix">RM</span>
                                <input style="border-radius:0 0.25em 0.25em 0" id="total_weight_value" type="text" class="form-control @error('total_weight_value') is-invalid @enderror" name="total_weight_value" value="{{ $shipment->total_weight_value }}">

                                @error('total_weight_value')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        @endif
                    </div>

                    <div class="form-group row">

                        <label for="draft_weight" class="col-md-4 col-xs-6 col-form-label text-md-right">Draft Weight</label>

                        <div class="col-md-3 col-xs-6" style="display:flex;white-space: nowrap">
                            <input id="draft_weight" style="border-radius:0.25em 0 0 0.25em;" type="text" class="form-control @error('draft_weight') is-invalid @enderror" name="draft_weight" value="{{ $shipment->draft_weight }}">
                                <span class="input-suffix">MT</span>
                                @error('draft_weight')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        @if($userLogged->hasRole('admin'))
                        <label for="draft_weight" style="display:flex;white-space: nowrap" class="col-md-1 col-xs-6 col-form-label text-md-right">Price /MT</label>

                        <div class="col-md-2 col-xs-6" style="display:flex;white-space: nowrap">
                            <span class="input-prefix">RM</span>
                                <input style="border-radius:0 0.25em 0.25em 0" id="draft_weight_value" type="text" class="form-control @error('draft_weight_value') is-invalid @enderror" name="draft_weight_value" value="{{ $shipment->draft_weight_value }}">

                                @error('draft_weight_value')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        @endif
                    </div>

                    @if($userLogged->hasRole('admin'))
                    <div class="form-group row">
                        <label for="purchase_value" class="col-md-4 col-form-label text-md-right">Purchase Value</label>

                        <div class="col-md-6">
                            <input id="purchase_value" type="text" class="form-control @error('purchase_value') is-invalid @enderror" name="purchase_value" value="{{ $shipment->purchase_value }}" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="sale" class="col-md-4 col-form-label text-md-right">Sale</label>

                        <div class="col-md-6">
                            <input id="sale" type="text" class="form-control @error('sale') is-invalid @enderror" name="sale" value="{{ $shipment->sale }}" readonly>
                        </div>
                    </div>
                    @endif

                    <div class="form-group row">
                        <label for="moisture" class="col-md-4 col-form-label text-md-right">Moisture</label>

                        <div class="col-md-6" style="display:flex">
                            <input id="moisture" style="border-radius:0.25em 0 0 0.25em" type="text" class="form-control @error('moisture') is-invalid @enderror" name="moisture" value="{{ $shipment->moisture }}">
                            <span class="input-suffix">%</span>
                            @error('moisture')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="remark" class="col-md-4 col-form-label text-md-right">Remark</label>

                        <div class="col-md-6">
                            <textarea id="remark" class="form-control @error('remark') is-invalid @enderror" name="remark" value="{{ $shipment->remark }}">{{ $shipment->remark }}</textarea>

                            @error('remark')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    @if($userLogged->hasRole('admin'))
                    <div class="form-group row">
                        <label for="created_at" class="col-md-4 col-form-label text-md-right">Created date</label>

                        <div class="col-md-6">
                            <input style="background-color:white" id="created_at" type="text" class="form-control @error('created_at') is-invalid @enderror" name="created_at" value="{{$shipment->created_at}}">

                            @error('created_at')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    @endif


                    <div class="form-group row">
                        <label for="total_ready_stock" class="col-md-4 col-form-label text-md-right">Total Ready Stock</label>

                        <div class="col-md-6" style="display:flex">
                            <input id="total_ready_stock" style="border-radius:0.25em 0 0 0.25em" type="text" class="form-control" name="total_ready_stock" value="{{isset($totalReadyStock)? $totalReadyStock:''}}" readonly>
                            <span class="input-suffix">MT</span>
                        </div>

                    </div>

                    <div class="form-group row" style="display:none">
                        <label for="stock_usage" class="col-md-4 col-form-label text-md-right"></label>

                        <div class="col-md-6">
                            <input id="stock_usage" type="text" class="form-control @error('stock_usage') is-invalid @enderror" name="stock_usage" value="" readonly>
                        </div>
                    </div>


                    <input type="hidden" name="fileUploadArray">
                    <input type="hidden" name="fileUploadRemove">
                </div>
                <div class="tab__content">

                    <span class="text-danger" id="fileError"></span>
                    <div style="display:flex; flex-direction: column;">

                    <div style="display:flex; flex-direction:row-reverse">

                        <button
                            type="button"
                            class="btn btn-outline-primary m-2"
                            data-toggle="modal"
                            data-target="#dropzoneModal"
                        >
                        Upload
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div style="overflow:auto">
                        <table class="table table-bordered mb-5">
                            <thead>
                                <tr class="table-success">
                                    <th scope="col">Image</th>
                                    <th scope="col">Caption</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="table_data">

                            </tbody>
                        </table>
                    </div>

                    </div>

                </div>

                </div>
            </div>

                <div class="form-group row mb-0" style="justify-content: end; margin: 0 1%;">
                    <div style="text-align:center">
                        <button id="btn_submit_shipment" type="button" class="btn btn-primary">
                            <span id="btn_create_text" class="" role="status" aria-hidden="true"></span>
                            Update
                            </button>
                        </button>
                    </div>
                </div>

            </form>



        </div>
        </div>
    </div>
    @else
        <div>You dont have permission</div>
    @endif


@include('partials.bottom_navbar')
@include('partials.modal.create_vehicle_modal')
@include('partials.modal.dropzone_modal')
@include('partials.modal.image_caption_modal')

@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <script>
        $(document).ready(function() {

            flatpickr("#created_at",{
                enableTime:true,
                enableSeconds:true,
            });

            $("#btn_submit_shipment").click(()=>{

                let totalReadyStock = Number("{{$totalReadyStock}}");
                let currentTW = Number("{{ $shipment->total_weight }}");
                let totalW = Number($("#total_weight").val());

                if(((totalReadyStock + currentTW) - totalW) >= 0){
                    $("#shipment_form").submit();

                    $("#btn_create_text").addClass('spinner-border spinner-border-sm');
                    $('#btn_submit_shipment').attr('disabled','disabled');

                    $("#shipment_form :input").prop('readonly', true);
                }else toastr.error("Fail edit, no enough of ready stock");

            })

            $("#tab1").prop("checked", true);

            // calculate purchase sale
            $("#draft_weight, #draft_weight_value").change(()=>{
                let valDW = $("#draft_weight").val();
                let valDWV = $("#draft_weight_value").val();

                if((valDW.length > 0) && (valDWV.length > 0)){
                    let purchase_value = valDW * valDWV;
                    $('#sale').val(purchase_value.toFixed(2));
                }else $('#sale').val('');
            })

            // calculate purchase value
            $("#total_weight, #total_weight_value").change(()=>{
                let valTW = $("#total_weight").val();
                let valTWV = $("#total_weight_value").val();

                if((valTW.length > 0) && (valTWV.length > 0)){
                    let purchase_value = valTW * valTWV;
                    $('#purchase_value').val(purchase_value.toFixed(2));
                }else $('#purchase_value').val('');
            })
        });

    </script>
    <script type="text/javascript">
        var fileArray = [];
        var fileArrayRemove = [];

        fileArray = {!! json_encode($arrayImageExist) !!}

        for(let i = 0; i < fileArray.length; i++){

            if(fileArray[i].mime == 'application/pdf'){
                var html = '<tr>';

                html += '<td><a target="_blank" href={{ URL::to('/') }}/upload_shipment/'+fileArray[i].folder_id+'/'+fileArray[i].path+'><label class="mr-2">'+fileArray[i].path+'</label><label class="imageTab_label ml-2" id="text_'+fileArray[i].name+'">'+fileArray[i].caption+'</label></a></td>';
                html += '<td><button type="button" data-path='+fileArray[i].path+' data-ori='+fileArray[i].path+' data-mime='+fileArray[i].mime+' name="'+fileArray[i].name+'" class="addCaption btn btn-primary">Add</button></td>';
                html += '<td><button type="button" name='+fileArray[i].path+' class="remove btn btn-danger">Delete</button></td></tr>';
            }else{
                var html = '<tr>';
                html += '<td><img width=200 height=150 src={{ URL::to('/') }}/upload_shipment/'+fileArray[i].folder_id+'/'+fileArray[i].path+'><label class="imageTab_label" id="text_'+fileArray[i].name+'">'+fileArray[i].caption+'</label></td>';
                html += '<td><button type="button" data-path='+fileArray[i].path+' data-ori='+fileArray[i].path+' data-mime='+fileArray[i].mime+' name="'+fileArray[i].name+'" class="addCaption btn btn-primary">Add</button></td>';
                html += '<td><button type="button" name='+fileArray[i].path+' class="remove btn btn-danger">Delete</button></td></tr>';
            }

            $('#table_data').prepend(html);

        }

        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone",
            {
                maxFilesize: 100,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                return time+file.name;
                },
                autoProcessQueue: false,
                maxFiles: 1,
                init: function() {
                    this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                    });
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf",
                addRemoveLinks: true,
                timeout: 50000,
                removedfile: function(file)
                {
                    return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
                },
                success: function(file, response)
                {
                    this.removeFile(file);
                    $('#fileError').text('');

                    // store to hidden input
                    let name = file.upload.filename;

                    let obj = {};
                    obj["path"] = response.img_path;
                    obj["caption"] = '';
                    obj["ori_path"] = response.ori_path;
                    obj["mime"] = response.mime;
                    obj["name"] = response.name;

                    fileArray.push(obj);

                    $("input[name='fileUploadArray']").val(JSON.stringify(fileArray));

                    if(response.mime == 'application/pdf'){
                        var html = '<tr>';
                        html += '<td><a target="_blank" href={{ URL::to('/') }}/upload/'+response.img_path+'><label class="mr-2">'+response.img_path+'</label><label class="imageTab_label ml-2" id="text_'+response.name+'"></label></a></td>';
                        html += '<td><button type="button" data-path='+response.img_path+' data-ori='+response.ori_path+' data-mime='+response.mime+' name="'+response.name+'" class="addCaption btn btn-primary">Add</button></td>';
                        html += '<td><button type="button" name='+response.img_path+' id="btn_remove_'+response.name+'" class="remove btn btn-danger">Delete</button></td></tr>';
                    }else{
                        var html = '<tr>';
                        html += '<td><img width=200 height=150 src={{ URL::to('/') }}/upload/'+response.img_path+'><label class="imageTab_label" id="text_'+response.name+'"></label></td>';
                        html += '<td><button type="button" data-path='+response.img_path+' data-ori='+response.ori_path+' data-mime='+response.mime+' name="'+response.name+'" class="addCaption btn btn-primary">Add</button></td>';
                        html += '<td><button type="button" name='+response.img_path+' id="btn_remove_'+response.name+'" class="remove btn btn-danger">Delete</button></td></tr>';
                    }

                    $('#table_data').prepend(html);

                },
                error: function(file, response)
                {
                    if(response.errors) {
                        this.removeFile(file);
                        $('#fileError').text(response.errors.file);
                    }
                    return false;
                }
            });

            $('#uploadFile').click(function(){
                myDropzone.processQueue();
                myDropzone.removeAllFiles();
            });

            $('#table_data').on('click', '.remove', function () {

                // Getting all the rows next to the
                // row containing the clicked button
                var child = $(this).closest('tr').nextAll();

                fileArrayRemove.push($(this).attr('name'))
                $("input[name='fileUploadRemove']").val(JSON.stringify(fileArrayRemove));

                // Removing the current row.
                $(this).closest('tr').remove();

            });

            $('#table_data').on('click', '.addCaption', function () {

                var child = $(this).closest('tr').nextAll();
                let attName = $(this).attr('name');
                let attPath = $(this).attr('data-path');
                $('#imageCaptionModal').modal('show');
                $('#caption').val('');
                $('#caption').attr('name',attName);
                $('#caption').attr('data-path',attPath);

            });

            $('#addCaptionBtn').click(()=>{

                let captionContent = $('#caption').val();
                let captionText = $('#caption').attr('name');
                let path = $('#caption').attr('data-path');

                if(captionText != null && captionText !=''){

                    // captionContent = captionContent.replace(/\s/g, '');
                    let targetid = '#text_'+captionText;
                    $(targetid).text(captionContent);

                    fileArray.find(function(o){
                        if(o.path == path) return o.caption = captionContent;
                    })
                    $("input[name='fileUploadArray']").val(JSON.stringify(fileArray));
                    console.log(fileArray)
                }

            });

            function removeRow(imageName){

                var index = fileArray.findIndex(function(o){
                    return o.path == imageName;
                })
                if (index !== -1) fileArray.splice(index, 1);

                $("input[name='fileUploadArray']").val(JSON.stringify(fileArray));

                $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                },
                        type: 'POST',
                        url: '{{ route("shipment-image-delete") }}',
                        data: {filename: imageName},
                        success: function (data){
                            console.log("File has been successfully removed!!");
                        },
                        error: function(e) {
                            console.log(e);
                }});
            }

            $("#draft_weight, #total_weight, #draft_weight_value, #total_weight_value, #moisture").on("input", function(evt) {
                var self = $(this);
                self.val(self.val().replace(/[^0-9\.]/g, ''));
                if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
                {
                    evt.preventDefault();
                }
            });

// https://www.geeksforgeeks.org/how-to-dynamically-add-remove-table-rows-using-jquery/


    </script>
@endsection
