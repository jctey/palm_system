@extends('layouts.app')

@section('content')

    @if($userLogged->hasRole('admin') || $userLogged->hasRole('manager'))
    <div class="container" style="padding-bottom:5%">
        <div class="card">
            <div class="card-header" style="display:flex; justify-content:space-between">
                <div style="align-self: center;">Shipment list</div>
                <div>
                    <a
                        class="btn btn-outline-primary"
                        style="box-shadow: 2px 3px 6px 4px #dddddd;"
                        href="{{ route('shipment-create') }}"
                    >
                        Create
                    </a>

                </div>
            </div>

            <div class="card-body" style="overflow-x:auto">

                <div class="panel-body">
                    <table class="table table-bordered" id="shipment-table" style="width:100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Ship Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
    @else
    <div class="container" style="padding-bottom:5%">
        <div class="card">
            <div class="card-header" style="display:flex; justify-content:space-between">
                <div style="align-self: center;">You dont have permission</div>
            </div>
        </div>
    </div>


    @endif


@include('partials.bottom_navbar')
@include('partials.modal.confirm_modal')


@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">

@endsection

@section('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>

<script>

        var table = $('#shipment-table').DataTable({
        processing: true,
        serverSide: true,
        bLengthChange: true,
        ajax: {
            url:'{{ route('shipment-list-datatable') }}',
            data: function (d) {
            }
        },

        columns: [
            { data: 'DT_RowIndex', name: 'index',searchable: false },
            { data: 'ship_name', name: 'ship_name' },
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: false
            },
        ],
        order: [[1, 'asc']],

        });

        $(document).ready(function () {


            $('#confirmModal').on('shown.bs.modal', function(e) {

                //get data-id attribute of the clicked element

                let type = $(e.relatedTarget).data('modaltype');

                if(type == 'shipment'){

                    let shipment_id = $(e.relatedTarget).data('shipmentid');

                    let url = '{{ route("shipment-delete", ":id") }}';
                    url = url.replace(':id', shipment_id);
                    $('#confirm_delete_form').attr('action', url);
                }else return false


            });

        });
</script>


@endsection