@extends('layouts.app')

@section('content')
<div class="container" style="padding-bottom:5%">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="dashboard_wrapper_box">
                    @if($user->user_verified == true)
                        @if(!$user->hasRole('clerk'))
                            <div class="dashboard_wrapper_left_content">
                                <div class="m-2" style="align-self: center;">
                                    <input id="dpicker" type="text" class="form-control form-control-sm" value=""
                                        data-type="daterange-picker" placeholder="select date" style="display:none">
                                </div>
                                <div class="m-2" style="display: flex;justify-content: end">
                                    <button id="btn_clear_filter_dashboard" class="m-2">Clear Filter</button>
                                    <button id="btn_filter_dashboard" class="m-2">Apply Filter</button>
                                </div>
                            </div>
                            <div id="rerender_right_box" style="display: contents;">
                                @include('partials.dashboard.collapse')
                            </div>
                        @else
                        <div class="container" style="padding-bottom:5%">
                            <div class="card">
                                <div class="card-header" style="display:flex; justify-content:space-between">
                                    <div style="align-self: center;">You dont have permission</div>
                                </div>
                            </div>
                        </div>
                        @endif


                    @else
                    <div> Your Account are not yet verified by admin </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
$( document ).ready(function() {
    const $daterangePicker = $('[data-type="daterange-picker"]');
    var DateSelected;

    initFlatPicker();

    function initFlatPicker(){
        // today
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' +dd;
        //

        if ($daterangePicker.length > 0) {
            $daterangePicker.flatpickr({
                mode: 'range',
                inline: true,
                dateFormat: "Y-m-d",
                defaultDate: today+" to "+today,
                onChange: function(selectedDates, dateStr, instance) {

                },

            });
        }
        filterSubmit()
    }


    $('#btn_filter_dashboard').click((e)=>{
        e.preventDefault();
        filterSubmit();
    })

    function filterSubmit(){
        let dateSelected = $('#dpicker').val();
        dateSelected = dateSelected.replace(/\s+/g, '')
        let dateSelectedArr = dateSelected.split('to');

        if(dateSelectedArr == undefined) alert('Please select date before filter');
        else{

            $.ajax({
                method: "GET",
                url: "{{route('filter')}}",
                data: {
                    date:dateSelectedArr
                },
                success: function (response) {
                    $('.collapse').collapse('show');
                    $('#rerender_right_box').html(response)
                    rerenderButton();
                },
            });

        }
    }


    $('#btn_clear_filter_dashboard').click((e)=>{

        $daterangePicker.flatpickr().clear();
        initFlatPicker();
        DateSelected=[];
        e.preventDefault();
        filterSubmit()
        // $.ajax({
        //     method: "GET",
        //     url: "{{route('filter')}}",
        //     data: {
        //         refresh:true
        //     },
        //     success: function (response) {
        //         $('.collapse').collapse('show');
        //         $('#rerender_right_box').html(response)
        //     },
        // });
    })

    rerenderButton();

    function rerenderButton(){

        detectSpanLength()

        $('#btn_to_shipment').on('click', function(){
            $('#incoming_wrapper').addClass('d-none');
            $('#shipment_wrapper').removeClass('d-none');

            let obj = document.getElementById("total_shipment");
            let objvalue = $('#total_shipment').text();

            animateValue(obj, 0, objvalue, 1500);

        });

        $('#btn_to_incoming').on('click', function(){
            $('#incoming_wrapper').removeClass('d-none');
            $('#shipment_wrapper').addClass('d-none');
        });

        $('#btn_view_detail').on('click', function() {
            $(this).addClass('d-none');
            $('#btn_go_back').removeClass('d-none');
            $('#statistic').addClass('d-none');
            $('#report_detail').removeClass('d-none');
            $('#btn_to_shipment').css('opacity',0);
        });
        $('#btn_go_back').on('click', function() {
            $(this).addClass('d-none');
            $('#btn_view_detail').removeClass('d-none');
            $('#report_detail').addClass('d-none');
            $('#statistic').removeClass('d-none');
            $('#btn_to_shipment').css('opacity',1);
        });

        $('#btn_view_detail_shipment').on('click', function() {
            $(this).addClass('d-none');
            $('#btn_go_back_shipment').removeClass('d-none');
            $('#statistic_shipment').addClass('d-none');
            $('#report_detail_shipment').removeClass('d-none');
            $('#btn_to_incoming').css('opacity',0);
        });
        $('#btn_go_back_shipment').on('click', function() {
            $(this).addClass('d-none');
            $('#btn_view_detail_shipment').removeClass('d-none');
            $('#report_detail_shipment').addClass('d-none');
            $('#statistic_shipment').removeClass('d-none');
            $('#btn_to_incoming').css('opacity',1);
        });

        $('.toggle_btn').on('click', function() {
            //$(this).toggleClass('fa-chevron-down fa-chevron-up');
            if($(this).find('i').hasClass("fa fa-chevron-up")){
                $(this).find('i').removeClass("fa fa-chevron-up").addClass("fa fa-chevron-down")
            }else{
                $(this).find('i').removeClass("fa fa-chevron-down").addClass("fa fa-chevron-up")
            }
        });

        // loading animation  ---value *0100
        let processed_loadingbar_value = $('#progress-bar-processed').attr('data-totalP');
        let unprocessed_loadingbar_value = $('#progress-bar-unprocessed').attr('data-totalU');
        LoadingBarAnimate('#progress-bar-processed',processed_loadingbar_value);
        LoadingBarAnimate('#progress-bar-unprocessed',unprocessed_loadingbar_value);

    }


    function LoadingBarAnimate(targetId,value){
        let max = $(targetId).attr('aria-valuemax');
        $(""+targetId+"-text").text(value);

        value = ((value / max) *100).toFixed(2);;

        let percent = ""+value+"%";
        $(targetId).animate({width: percent}, 3000);
    }

    function animateValue(obj, start, end, duration) {

        if(end != null && end != undefined && end != ''){
            let startTimestamp = null;
            const step = (timestamp) => {
                if (!startTimestamp) startTimestamp = timestamp;
                const progress = Math.min((timestamp - startTimestamp) / duration, 1);
                obj.innerHTML = Math.floor(progress * (end - start) + start);
                if (progress < 1) {
                window.requestAnimationFrame(step);
                }
            };
                window.requestAnimationFrame(step);
        }

    }

    function detectSpanLength(){

        if($('#total_amount').text().length > 12) $('#total_amount').css('font-size','x-large');

        if($('#total_weight_stock').text().length > 10) $('#total_weight_stock').css('font-size','large');

        if($('#total_purchase').text().length > 10) $('#total_purchase').css('font-size','large');
        else if($('#total_purchase').text().length > 5) $('#total_purchase').css('font-size','x-large');

        if($('#total_sale').text().length > 10) $('#total_sale').css('font-size','large');
        else if($('#total_sale').text().length > 5) $('#total_sale').css('font-size','x-large');

        if($('#total_ready_stock').text().length > 10) $('#total_ready_stock').css('font-size','large');
        else if($('#total_ready_stock').text().length > 5) $('#total_ready_stock').css('font-size','x-large');


    }

});
</script>
@endsection