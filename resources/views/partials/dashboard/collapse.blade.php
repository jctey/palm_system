<div id="dashboard_right_box" class="dashboard_wrapper_right_content">
    <div id="incoming_wrapper">
        @if(isset($dataFiltered))
            <div id="right_upper_upcoming" class="dashboard_wrapper_right_content_upper">
                @if(isset($userLogged))
                    @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                    <div>
                        <span>TOTAL VALUE</span>
                        <div style="display:flex">RM <span id="total_amount" class="text_rm">{{$total_amount}}</span></div>
                    </div>
                    @endif
                @endif
                <div>
                    <span>TOTAL WEIGHT IN STOCK</span>
                    <div style="display:flex"><span id="total_weight_stock" class="text_rm">{{$total_weight_stock}}</span><span>MT</span></div>
                </div>

                <div style="display:flex">

                    <div class="dashboard_wrapper_right_content_upper_inner">
                        <div style="text-align: end;"><i class="fa fa-calendar mr-2" aria-hidden="true"></i>{{$date_range}}</div>
                        <div style="display: flex;">
                            <button id="btn_to_shipment" style="height:fit-content; align-self: flex-end;margin: 0 1em;">Shipment
                            <i class="fa fa-exchange" aria-hidden="true"></i>
                            </button>
                            <button id="btn_view_detail">View Detail</button>
                            <button id="btn_go_back" class="d-none">Go Back</button>
                        </div>
                    </div>

                </div>

            </div>

            <div id="statistic" class="dashboard_wrapper_right_content_bottom">
                <h5>STATISTICS</h5>

                <span>Total processed</span>
                <div style="display:flex">
                    <div class="progress m-1" style="width: 100%;">
                        <div id="progress-bar-processed" data-totalP ="{{$total_processed}}" class="progress-bar bg-success" role="progressbar" aria-valuemin="0" aria-valuemax="{{$total_processed+$total_unprocessed}}">
                        </div>
                    </div>
                    <span id="progress-bar-processed-text" style="align-self: center; margin: 0 0.5em;font-weight: bold;"></span>
                </div>


                <span>Total unprocessed</span>
                <div style="display:flex">
                    <div class="progress m-1" style="width: 100%;">
                        <div id="progress-bar-unprocessed" data-totalU ="{{$total_unprocessed}}" class="progress-bar bg-danger" role="progressbar" aria-valuemin="0" aria-valuemax="{{$total_processed+$total_unprocessed}}">
                        </div>
                    </div>
                    <span id="progress-bar-unprocessed-text" style="align-self: center; margin: 0 0.5em;font-weight: bold;"></span>
                </div>

            </div>
            <div id="report_detail" class="dashboard_wrapper_right_content_bottom d-none">
                <h5>REPORT DETAILS</h5>

                <div id="accordion">
                    @if(count($dataFiltered)>0)
                        @foreach($dataFiltered as $dataList)
                        <div class="card">
                            <div>
                                <h5 class="mb-0">
                                    <div style="display:flex; align-items: center; margin: 0 1em;">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <div style="margin: 0 1em;">{{$dataList->date}}</div>
                                    </div>
                                    <div style="display:flex; align-items: center;">
                                        <div style="display:flex">
                                            <div style="margin: 0 1em;">
                                                Total Processed :<span class="text_processed">{{$dataList->each_total_processed}}</span>
                                            </div>
                                            <div style="margin: 0 1em;">
                                                Total Unprocessed :<span class="text_unprocessed">{{$dataList->each_total_unprocessed}}</span>
                                            </div>
                                        </div>
                                        <button class="toggle_btn btn btn-link" data-toggle="collapse" data-target="#collapseOne{{ $loop->index }}" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        </button>
                                    </div>

                                </h5>
                            </div>

                            <div id="collapseOne{{ $loop->index }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                @if(isset($dataList->data))
                                    @if(count($dataList->data)>0)
                                        @foreach($dataList->data as $d_detail)
                                        <div class="card-body" style="padding:0; margin-bottom:1em">
                                            <div class="collapse-cardbody-wrapper">
                                                <div class="collapse-cardbody-header">
                                                    <div class="header-left">
                                                        <div class="header-left-index">{{ $loop->index +1 }}</div>
                                                        <div class="header-left-truckNum">{{$d_detail->truck_id}}</div>
                                                    </div>
                                                    <div class="header-right">
                                                        <div class="header-right-markProcessed">
                                                            @if(($d_detail->processed_status == true) && (date('Y-m-d', strtotime($d_detail->processed_date)) != date('Y-m-d', strtotime($dataList->date))))
                                                            Mark Processed :
                                                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                                            {{date('Y-m-d', strtotime($d_detail->processed_date))}}
                                                            @endif
                                                        </div>

                                                        <div class="header-right-issued">issued by {{$d_detail->issued_by}}</div>

                                                        <div class="header-right-processed-status {{($d_detail->processed_status == true)&&(date('Y-m-d', strtotime($dataList->date))==date('Y-m-d', strtotime($d_detail->processed_date))) ? 'color-status-processed' :'color-status-unprocessed'}}">
                                                            {{($d_detail->processed_status == true)&&(date('Y-m-d', strtotime($dataList->date))==date('Y-m-d', strtotime($d_detail->processed_date))) ? 'Processed' :'Unprocessed'}}
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="collapse-cardbody-detail">
                                                    <div style="width:100%">

                                                        <div class="row">
                                                            <div class="col-md-2 col-6 d-grey">Ticket No </div>
                                                            <span class="col-md-2 col-6">{{$d_detail->ticket_no}}</span>
                                                            <div class="col-md-2 col-6 d-grey">Supplier </div>
                                                            <span class="col-md-6 col-6">{{$d_detail->supplier}}</span>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-2 col-6 d-grey">Weight </div>
                                                            <span class="col-md-2 col-6">
                                                                @if(!empty($d_detail->weight))
                                                                    {{$d_detail->weight}} MT
                                                                @endif
                                                            </span>
                                                            <div class="col-md-2 col-6 d-grey">Transporter </div>
                                                            <span class="col-md-6 col-6">{{$d_detail->transporter}}</span>
                                                        </div>

                                                        <div class="row">
                                                            <div style="white-space: nowrap;" class="col-md-2 col-6 d-grey">Wastage </div>
                                                            <span class="col-md-2 col-6">
                                                                @if(!empty($d_detail->wastage))
                                                                     {{$d_detail->wastage}} MT
                                                                @endif
                                                            </span>
                                                            <div class="col-md-2 col-6 d-grey">Product(s) </div>
                                                            <span class="col-md-6 col-6">{{$d_detail->product}}</span>
                                                        </div>

                                                        <div class="row">
                                                        @if(isset($userLogged))
                                                            @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                                                            <div class="col-md-2 col-6 d-grey"> Goods Price</div>
                                                            <span class="col-md-2 col-6">

                                                                @if(!empty($d_detail->goods_price))
                                                                    RM {{$d_detail->goods_price}}
                                                                @endif
                                                            </span>
                                                            @endif
                                                        @endif
                                                            <div style="white-space: nowrap;" class="col-md-2 col-6 d-grey">Moisture</div>
                                                            <span class="col-md-6 col-6">
                                                                @if(!empty($d_detail->moisture))
                                                                    {{$d_detail->moisture}} %
                                                                @endif
                                                            </span>
                                                        </div>

                                                        <div class="row">
                                                        @if(isset($userLogged))
                                                            @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                                                            <div style="white-space: nowrap;" class="col-md-2 col-6 d-grey">Transport Fee </div>
                                                            <span class="col-md-2 col-6">
                                                                @if(!empty($d_detail->transport))
                                                                    RM {{$d_detail->transport}}
                                                                @endif
                                                            </span>
                                                            @endif
                                                        @endif
                                                            <div class="col-md-2 col-6 d-grey">Remark </div>
                                                            <span style="overflow-wrap: anywhere;" class="col-md-6 col-6">{{$d_detail->remark}}</span>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="collapse-cardbody-doc">
                                                    <div style="width: 100%;">
                                                        <div class="row">
                                                            <div class="col-md-3 col-6 d-grey">Documents </div>
                                                            <div class="col-md-9 col-6">
                                                                @if($d_detail->images)
                                                                    @if(count($d_detail->images)>0)
                                                                        @foreach($d_detail->images as $img)
                                                                        <a target="_blank" href="{{asset('upload/'.$img->processed_id.'/'.$img->image_url)}}">{{$img->caption ? $img->caption .'('. $img->mime_type.')':'No caption'.'('. $img->mime_type.')'}}</a>
                                                                            @if( !$loop->last)
                                                                                ,
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                @else
                                    <div class="card-body" style="padding:0">
                                        <div class="collapse-cardbody-wrapper">
                                            <div class="collapse-cardbody-header">
                                                <div class="header-left">
                                                    <div>No Record for today</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>

            </div>
        @endif
    </div>

    <!-- shipment -->
    <div id="shipment_wrapper" class="d-none">
        @if(isset($dataFilteredShipment))
        <div id="right_upper_upcoming" class="dashboard_wrapper_right_content_upper">
            @if(isset($userLogged))
                @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                <div>
                    <span>TOTAL PURCHASE VALUE</span>
                    <div style="display:flex">RM <span id="total_purchase" class="text_rm">{{$total_purchase}}</span></div>
                </div>
                <div>
                    <span>TOTAL SALE</span>
                    <div style="display:flex">RM<span id="total_sale" class="text_rm">{{$total_sale}}</span></div>
                </div>
                @endif
            @endif
            <div>
                <span>TOTAL READY STOCK</span>
                <div style="display:flex"><span id="total_ready_stock" class="text_rm">{{$total_ready_stock}}</span>MT</div>
            </div>

            <div style="display:flex">

                <div class="dashboard_wrapper_right_content_upper_inner">
                    <div style="text-align: end;"><i class="fa fa-calendar mr-2" aria-hidden="true"></i>{{$date_range}}</div>
                    <div style="display: flex;">
                        <button id="btn_to_incoming" style="height:fit-content; align-self: flex-end;margin: 0 1em;">Processed
                        <i class="fa fa-exchange" aria-hidden="true"></i>
                        </button>
                        <button id="btn_view_detail_shipment">View Detail</button>
                        <button id="btn_go_back_shipment" class="d-none">Go Back</button>
                    </div>
                </div>

            </div>

        </div>

        <div id="statistic_shipment" class="dashboard_wrapper_right_content_bottom">
            <h5>STATISTICS</h5>

            <span>Total shipment</span>
            <div style="display:flex">
                <span id="total_shipment" style="font-weight: bold;font-size: xx-large;">{{$total_shipment}}</span>
            </div>

        </div>
        <div id="report_detail_shipment" class="dashboard_wrapper_right_content_bottom d-none">
            <h5>REPORT DETAILS</h5>

            <!-- collapse -->
            <div id="accordion">
                @if(count($dataFilteredShipment)>0)
                    @foreach($dataFilteredShipment as $dataList)
                    <div class="card">
                        <div>
                        <h5 class="mb-0">
                            <div style="display:flex; align-items: center; margin: 0 1em;">
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                <div style="margin: 0 1em;">{{$dataList->date}}</div>
                            </div>
                            <div style="display:flex; align-items: center;">
                                <div style="display:flex">
                                    <div style="margin: 0 1em;">
                                    Total Shipment :<span class="text_processed">{{$dataList->each_total_shipment}}</span>
                                    </div>
                                </div>
                                <button class="toggle_btn btn btn-link" data-toggle="collapse" data-target="#collapseOne{{ $loop->index }}" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                </button>
                            </div>

                        </h5>
                        </div>

                        <div id="collapseOne{{ $loop->index }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        @if(isset($dataList->data))
                            @if(count($dataList->data)>0)
                                @foreach($dataList->data as $d_detail)
                                <div class="card-body" style="padding:0; margin-bottom:1em">
                                    <div class="collapse-cardbody-wrapper">
                                        <div class="collapse-cardbody-header">
                                            <div class="header-left">
                                                <div class="header-left-index">{{ $loop->index +1 }}</div>

                                            </div>
                                            <div class="header-right">


                                            </div>
                                        </div>
                                        <div class="collapse-cardbody-detail">
                                            <div style="width:100%">

                                                <div class="row">
                                                    <div class="col-md-3 col-6 d-grey">Ship Name </div>
                                                    <span class="col-md-3 col-6">{{$d_detail->ship_name}}</span>


                                                    <div style="white-space: nowrap;" class="col-md-3 col-6 d-grey">Moisture </div>
                                                    <span class="col-md-3 col-6">
                                                        @if(!empty($d_detail->moisture))
                                                            {{$d_detail->moisture}} %
                                                        @endif
                                                    </span>

                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-6 d-grey">Total Weight </div>
                                                    <span class="col-md-3 col-6">
                                                        @if(!empty($d_detail->total_weight))
                                                            {{$d_detail->total_weight}} MT
                                                        @endif
                                                    </span>
                                                    @if(isset($userLogged))
                                                        @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                                                        <div style="white-space: nowrap;" class="col-md-3 col-6 d-grey">Total Weight (Price/MT)</div>
                                                        <span class="col-md-3 col-6">
                                                            @if(!empty($d_detail->total_weight_value))
                                                                RM {{$d_detail->total_weight_value}}
                                                            @endif
                                                        </span>
                                                        @endif
                                                    @endif
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-6 d-grey">Draft Weight </div>
                                                    <span class="col-md-3 col-6">
                                                        @if(!empty($d_detail->draft_weight))
                                                            {{$d_detail->draft_weight}} MT
                                                        @endif
                                                    </span>
                                                    @if(isset($userLogged))
                                                        @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                                                        <div style="white-space: nowrap;" class="col-md-3 col-6 d-grey">Draft Weight (Price/MT)</div>
                                                        <span class="col-md-3 col-6">
                                                            @if(!empty($d_detail->draft_weight_value))
                                                                RM {{$d_detail->draft_weight_value}}
                                                            @endif
                                                        </span>
                                                        @endif
                                                    @endif
                                                </div>

                                                @if(isset($userLogged))
                                                    @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                                                    <div class="row">
                                                        <div class="col-md-3 col-6 d-grey">Purchase Value </div>
                                                        <span class="col-md-3 col-6">
                                                            @if(!empty($d_detail->purchase_value))
                                                                RM {{$d_detail->purchase_value}}
                                                            @endif
                                                        </span>
                                                        <div class="col-md-3 col-6 d-grey">Sale </div>
                                                        <span class="col-md-3 col-6">{{$d_detail->sale}}</span>
                                                    </div>
                                                    @endif
                                                @endif

                                                <div class="row">
                                                    <div class="col-md-3 col-6 d-grey">Remark </div>
                                                    <span style="overflow-wrap: anywhere;" class="col-md-9 col-6">{{$d_detail->remark}}</span>
                                                </div>


                                            </div>

                                        </div>
                                        <div class="collapse-cardbody-doc">
                                            <div style="width: 100%;">
                                                <div class="row">
                                                    <div class="col-md-3 col-6 d-grey">Documents </div>
                                                    <div class="col-md-9 col-6">
                                                    @if($d_detail->images)
                                                        @if(count($d_detail->images)>0)
                                                            @foreach($d_detail->images as $img)
                                                            <a target="_blank" href="{{asset('upload_shipment/'.$img->shipment_id.'/'.$img->image_url)}}">{{$img->caption ? $img->caption .'('. $img->mime_type.')':'No caption'.'('. $img->mime_type.')'}}</a>
                                                                @if( !$loop->last)
                                                                    ,
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            @endif
                        @else
                            <div class="card-body" style="padding:0">
                                <div class="collapse-cardbody-wrapper">
                                    <div class="collapse-cardbody-header">
                                        <div class="header-left">
                                            <div>No Record for today</div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif
                        </div>
                    </div>
                    @endforeach
                @endif


            </div>

            <!-- collapse -->
        </div>
        @endif
    </div>
</div>