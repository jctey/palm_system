<div class="col-md-6">
    <select name="truck" id="truck" class="form-control tags-selector" style="width:100%"
        @if(isset($isViewPage))
            disabled
        @endif
    >
        <option value=""></option>
        <option value="NEW">Add New</option>
        @if($trucks->count() > 0)
            @foreach($trucks as $truck)
                <option value="{{$truck->id}}"
                    @if(isset($processed->truck_id))
                        @if($truck->id == $processed->truck_id)
                            selected
                        @endif
                    @endif
                >
                    {{$truck->truck_plate_number}}
                </option>
            @endforeach
        @endif
    </select>
</div>