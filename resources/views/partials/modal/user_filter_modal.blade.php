<!-- Modal -->
<div class="modal fade" id="userFilterModalCenter" tabindex="-1" role="dialog" aria-labelledby="userFilterModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Filter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body" style="display:flex; flex-direction:column">
              <div>

                <div style="display:inline-flex; width:100%">
                  <label class="col-5">Email status: </label>
                  <div class="radio col-6" style="display:flex; flex-wrap:wrap; justify-content: space-between">
                    <label style="align-items:center">
                      <input type="radio" style="margin-right: 5px" name="verify" value="verify">Verified
                    </label>
                    <label style="align-items:center">
                      <input type="radio" style="margin-right: 5px" name="verify" value="unverify">Unverified
                    </label>
                  </div>
                </div>

                <div style="display:inline-flex; width:100%">
                  <label class="col-5">User status: </label>
                  <div class="radio col-6" style="display:flex; flex-wrap:wrap; justify-content: space-between">
                    <label style="align-items:center">
                      <input type="radio" style="margin-right: 5px" name="user_verified" value="user_verified">Verified
                    </label>
                    <label style=" align-items:center">
                      <input type="radio" style="margin-right: 5px" name="user_verified" value="user_unverified">Unverified
                    </label>
                  </div>

                </div>

                <div style="display:inline-flex; width:100%">

                  <label class="col-5">Role: </label>
                  <div class="radio col-6" style="display:flex; flex-wrap:wrap; justify-content: space-between">
                    <label style="align-items:center">
                      <input type="radio" style="margin-right: 5px" name="role" value="admin">Admin
                    </label>

                    <label style="align-items:center">
                      <input type="radio" style="margin-right: 5px" name="role" value="supervisor">Supervisor
                    </label>

                    <label style="align-items:center">
                      <input type="radio" style="margin-right: 5px" name="role" value="shareholder">Shareholder
                    </label>
                  </div>
                </div>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" id="filter_reset_btn" class="btn btn-secondary" data-dismiss="modal">Reset</button>
          <button type="submit" id="filter_btn" class="btn btn-primary" data-dismiss="modal">Filter</button>
        </div>

    </div>
  </div>
</div>
