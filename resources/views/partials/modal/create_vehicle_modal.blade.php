
<!-- Modal -->
<div class="modal fade" id="createVehicleModal" tabindex="-1" role="dialog" aria-labelledby="createVehicleModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <form id="confirm_create_vehicle_form" action="" method="post">
            <div class="modal-body">
                @csrf
                <p class="text-center">New Vehicle No</p>
                <input id="new_vehicle_no" type="text" class="form-control @error('new_vehicle_no') is-invalid @enderror" name="new_vehicle_no" value="{{ old('new_vehicle_no') }}" required>
                <div class="alert alert-danger" style="display:none"></div>

            </div>
            <div style="float:right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" id="vehicle_create" class="btn btn-danger">Create</button>
            </div>
        </form>
      </div>

    </div>
  </div>
</div>