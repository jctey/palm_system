
<!-- Modal -->
<div class="modal fade" id="dropzoneModal" tabindex="-1" role="dialog" aria-labelledby="dropzoneModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
      <h4 class="text-center font-weight-bold">File & Image Upload</h4>
          <div style="display:flex; flex-direction: row-reverse; margin:5px 0">
            <button type="button" class="btn btn-danger" data-dismiss="modal">X</button>
          </div>
          <form method="post" action="{{route('image-store')}}" enctype="multipart/form-data"
                      class="dropzone" id="dropzone">

          @csrf
          </form>
          <div style="display:flex; flex-direction: row-reverse; margin:5px 0">
            <button id="uploadFile" type="button" class="btn btn-primary" data-dismiss="modal">Upload</button>
          </div>
      </div>

    </div>
  </div>
</div>