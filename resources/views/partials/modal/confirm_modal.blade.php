
<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
        <form id="confirm_delete_form" action="" method="post">
            <div class="modal-body">
                @csrf
                @method('DELETE')
                <p class="text-center">Are you sure you want to delete <span style="font-weight:bold" id="confirm_email"></span> ?</p>

            </div>
            <div style="float:right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Yes, Delete</button>
            </div>
        </form>
      </div>

    </div>
  </div>
</div>