
<!-- Modal -->
<div class="modal fade" id="imageCaptionModal" tabindex="-1" role="dialog" aria-labelledby="imageCaptionModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-body">
      <h4 class="text-center font-weight-bold">Caption</h4>
          <div style="display:flex; flex-direction: row-reverse; margin:5px 0">
            <button type="button" class="btn btn-danger" data-dismiss="modal">X</button>
          </div>

          <input id="caption" type="text" data-path="" class="form-control" name="" value="">

          <div style="display:flex; flex-direction: row-reverse; margin:5px 0">
            <button id="addCaptionBtn" type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
          </div>
      </div>

    </div>
  </div>
</div>