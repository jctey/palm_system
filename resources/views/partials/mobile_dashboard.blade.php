<div>
    <div id="incoming_wrapper_mobile">
        <div class="dashboard_top_wrap_m">

            <div style="display: flex;justify-content: space-between;">
                <div>
                    <i class="fa fa-calendar mr-2" aria-hidden="true"></i>{{isset($date_range)? $date_range : ''}}
                    <i id="editIconShipment" style="margin:0 1em" class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </div>
            </div>
            @if(isset($userLogged))
                @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                <div>
                    <span style="color:lightgrey">TOTAL VALUE</span>
                    <div style="display:flex;">RM <span id="total_value_m" class="text_rm">{{isset($total_amount)?$total_amount:''}}</span></div>
                </div>
                @endif
            @endif
            <div>
                <div>
                    <span style="color:lightgrey">TOTAL WEIGHT IN STOCK</span>
                </div>
                <div style="display:flex; justify-content: space-between;">
                    <div style="display:flex;"><span id="total_weight_in_stock_m" class="text_rm">{{isset($total_weight_stock)?$total_weight_stock:''}}</span><span style="display:flex; align-items: center;">MT</span></div>
                    <button id="btn_to_shipment_mobile" style="height:fit-content; align-self: center">Shipment
                    <i class="fa fa-exchange" aria-hidden="true"></i>
                    </button>
                </div>

            </div>

        </div>

        <div class="dashboard_bottom_wrap_m">
            <div id="statistic_m">
                <div style="display:flex; justify-content: space-between;">
                    <h5 style="color:lightslategrey">STATISTICS</h5>
                    <h6 class="see_details" style="color:lightslategrey">See Details<i class="fa fa-angle-right ml-2" aria-hidden="true"></i></h6>
                </div>

                <div style="margin: 1em 0;">
                    <span>Total Processed</span>
                    <div style="display:flex; margin:0.5em 0">
                        <div class="progress m-1" style="width: 100%;">
                            <div id="progress-bar-processed" data-totalP ="{{isset($total_processed)?$total_processed:''}}" class="progress-bar bg-success" role="progressbar" aria-valuemin="0" aria-valuemax="{{isset($total_processed)&&isset($total_unprocessed)?$total_processed + $total_unprocessed:''}}">
                            </div>
                        </div>
                        <span id="progress-bar-processed-text" style="align-self: center; margin: 0 0.5em;font-weight: bold;"></span>
                    </div>
                </div>

                <div style="margin: 1em 0;">
                    <span>Total Unprocessed</span>
                    <div style="display:flex; margin:0.5em 0">
                        <div class="progress m-1" style="width: 100%;">
                            <div id="progress-bar-unprocessed" data-totalU ="{{isset($total_unprocessed)?$total_unprocessed:''}}" class="progress-bar bg-danger" role="progressbar" aria-valuemin="0" aria-valuemax="{{isset($total_processed)&&isset($total_unprocessed)?$total_processed + $total_unprocessed:''}}">
                            </div>
                        </div>
                        <span id="progress-bar-unprocessed-text" style="align-self: center; margin: 0 0.5em;font-weight: bold;"></span>
                    </div>
                </div>
            </div>
            <div id="details_m" class="d-none">
                <div style="display:flex; justify-content: space-between;">
                    <h5 style="color:lightslategrey">REPORT DETAILS</h5>
                    <h6 class="go_back" style="color:lightslategrey"><i class="fa fa-reply mr-2" aria-hidden="true"></i>Back</h6>
                </div>

                <div id="accordion">
                    @if(isset($dataFiltered))
                        @if(count($dataFiltered)>0)
                            @foreach($dataFiltered as $dataList)
                            <div class="card" style="border-top:none; border-left:none; border-right:none; margin-top: 0.5em;">
                                <div>
                                    <h5 class="mb-0" style="flex-direction: column;">
                                        <div style="display:flex; align-items: center; margin: 0.5em 1em;">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <div style="margin: 0 1em;">{{$dataList->date}}</div>
                                        </div>
                                        <div style="display:flex; align-items: center;justify-content: space-between;">
                                            <div style="display:flex">
                                                <div style="margin-left:1em;">
                                                Total Processed :<span class="text_processed" style="margin-left: 0.5em">{{$dataList->each_total_processed}}</span>
                                                </div>
                                                <div style="margin-left:1em;">
                                                Total Unprocessed :<span class="text_unprocessed" style="margin-left: 0.5em">{{$dataList->each_total_unprocessed}}</span>
                                                </div>
                                            </div>
                                            <button class="toggle_btn btn btn-link" data-toggle="collapse" data-target="#collapseOne{{ $loop->index }}" aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                            </button>
                                        </div>

                                    </h5>
                                </div>

                                <div id="collapseOne{{ $loop->index }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="margin-bottom:20%">
                                    @if(isset($dataList->data))
                                        @if(count($dataList->data)>0)
                                            @foreach($dataList->data as $d_detail)
                                            <div class="card-body" style="padding:0; margin-bottom:1em">
                                                <div class="collapse-cardbody-wrapper">
                                                    <div class="collapse-cardbody-header">
                                                        <div class="header-left">
                                                            <div class="header-left-index">01</div>
                                                            <div class="header-left-truckNum">{{$d_detail->truck_id}}</div>
                                                        </div>
                                                        <div class="header-right">

                                                            <div class="header-right-processed-status {{($d_detail->processed_status == true)&&(date('Y-m-d', strtotime($dataList->date))==date('Y-m-d', strtotime($d_detail->processed_date))) ? 'color-status-processed' :'color-status-unprocessed'}}">
                                                                {{($d_detail->processed_status == true)&&(date('Y-m-d', strtotime($dataList->date))==date('Y-m-d', strtotime($d_detail->processed_date))) ? 'Processed' :'Unprocessed'}}
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div style="display:flex; justify-content:space-between">
                                                        <div style="font-style: oblique; color: darkgrey; margin:0 0.5em">issued by {{$d_detail->issued_by}}</div>
                                                        <div class="header-right-markProcessed" style="color:green; margin:0 0.5em">
                                                            @if(($d_detail->processed_status == true) && (date('Y-m-d', strtotime($d_detail->processed_date)) != date('Y-m-d', strtotime($dataList->date))))
                                                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                                            {{date('Y-m-d', strtotime($d_detail->processed_date))}}
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="collapse-cardbody-detail">
                                                        <div style="width:100%; font-size: small">

                                                            <div class="row" >
                                                                <div class="col-4 pr-0 d-grey">Ticket No </div>
                                                                <span class="col-3 pl-0 pr-1">{{$d_detail->ticket_no}}</span>
                                                                <div class="col-2 pl-0 pr-0 d-grey">Moisture</div>
                                                                <span class="col-3 pl-2 pr-0">
                                                                    @if(!empty($d_detail->moisture))
                                                                        {{$d_detail->moisture}}%
                                                                    @endif
                                                                </span>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-4 pr-0 d-grey">Transport Fee </div>
                                                                <span class="col-3 pl-0 pr-1">
                                                                    @if(!empty($d_detail->transport))
                                                                        RM{{$d_detail->transport}}
                                                                    @endif
                                                                </span>
                                                                <div class="col-2 pl-0 pr-0 d-grey">Weight</div>
                                                                <span class="col-3 pl-2 pr-0">
                                                                    @if(!empty($d_detail->weight))
                                                                        {{$d_detail->weight}}MT
                                                                    @endif
                                                                </span>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-4 pr-0 d-grey"> Good Price</div>
                                                                <span class="col-3 pl-0 pr-1">
                                                                    @if(!empty($d_detail->goods_price))
                                                                        RM {{$d_detail->goods_price}}
                                                                    @endif
                                                                </span>
                                                                <div class="col-2 pl-0 pr-0 d-grey">Wastage </div>
                                                                <span class="col-3 pl-2 pr-0">
                                                                    @if(!empty($d_detail->wastage))
                                                                        {{$d_detail->wastage}} MT
                                                                    @endif
                                                                </span>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-4 d-grey">Transporter </div>
                                                                <span class="col-8 pl-0">{{$d_detail->transporter}}</span>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-4 d-grey">Product(s) </div>
                                                                <span class="col-8 pl-0">{{$d_detail->product}}</span>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-4 d-grey">Supplier </div>
                                                                <span class="col-8 pl-0">{{$d_detail->supplier}}</span>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-4 d-grey">Remark </div>
                                                                <span class="col-8 pl-0">{{$d_detail->remark}}</span>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="collapse-cardbody-doc">
                                                        <div style="width: 100%;">
                                                            <div class="row">
                                                                <div class="col-4 d-grey">Documents </div>
                                                                <div class="col-8 pl-0">
                                                                @if($d_detail->images)
                                                                    @if(count($d_detail->images)>0)
                                                                        @foreach($d_detail->images as $img)
                                                                        <a target="_blank" href="{{asset('upload/'.$img->processed_id.'/'.$img->image_url)}}">{{$img->caption ? $img->caption .'('. $img->mime_type.')':'No caption'.'('. $img->mime_type.')'}}</a>
                                                                            @if( !$loop->last)
                                                                                ,
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            @endforeach
                                        @endif
                                    @else
                                        <div class="card-body" style="padding:0">
                                            <div class="collapse-cardbody-wrapper">
                                                <div class="collapse-cardbody-header">
                                                    <div class="header-left">
                                                        <div>No Record for today</div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        @endif
                    @endif
                </div>

            </div>
        </div>
    </div>

    <div id="shipment_wrapper_mobile" class="d-none">
        <div class="dashboard_top_wrap_m">

            <div style="display: flex;justify-content: space-between;">
                <div>
                    <i class="fa fa-calendar mr-2" aria-hidden="true"></i>{{isset($date_range)? $date_range : ''}}
                    <i id="editIconProcessed" style="margin:0 1em" class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </div>
            </div>
            @if(isset($userLogged))
                @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                <div>
                    <span style="color:lightgrey">TOTAL PURCHASE VALUE</span>
                    <div style="display:flex;">RM <span id="total_purchase_m" class="text_rm">{{isset($total_purchase)?$total_purchase:'0'}}</span></div>
                </div>
                <div>
                    <span style="color:lightgrey">TOTAL SALE</span>

                    <div style="display:flex;">RM <span id="total_sale_m" class="text_rm">{{isset($total_sale)?$total_sale:'0'}}</span></div>
                </div>
                @endif
            @endif
            <div>
                <div>
                    <span style="color:lightgrey">TOTAL READY STOCK</span>
                </div>
                <div style="display:flex; justify-content: space-between;">
                    <div style="display:flex;"><span id="total_ready_stock_m" class="text_rm">{{isset($total_ready_stock)?$total_ready_stock:'0'}}</span><span style="display:flex; align-items: center;">MT</span></div>
                    <button id="btn_to_incoming_mobile" style="height:fit-content; align-self: center">Processed
                    <i class="fa fa-exchange" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="dashboard_bottom_wrap_m">
            <div id="statistic_shipment_m">
                <div style="display:flex; justify-content: space-between;">
                    <h5 style="color:lightslategrey">STATISTICS</h5>
                    <h6 class="see_details" style="color:lightslategrey">See Details<i class="fa fa-angle-right ml-2" aria-hidden="true"></i></h6>
                </div>

                <div style="margin: 1em 0;">
                    <span>Total Shipment</span>
                    <div style="display:flex; margin:0.5em 0">
                    <span id="total_shipment" style="font-weight: bold;font-size: xx-large;">{{isset($total_shipment)?$total_shipment:''}}</span>
                    </div>
                </div>
            </div>
            <div id="details_shipment_m" class="d-none">
                    <div style="display:flex; justify-content: space-between;">
                        <h5 style="color:lightslategrey">REPORT DETAILS</h5>
                        <h6 class="go_back" style="color:lightslategrey"><i class="fa fa-reply mr-2" aria-hidden="true"></i>Back</h6>
                    </div>

                    <!-- collapse -->
                    <div id="accordion">
                        @if(isset($dataFilteredShipment))
                        @if(count($dataFilteredShipment)>0)
                            @foreach($dataFilteredShipment as $dataList)
                            <div class="card" style="border-top:none; border-left:none; border-right:none; margin-top: 0.5em;">
                                <div>
                                <h5 class="mb-0" style="flex-direction: column;">
                                    <div style="display:flex; align-items: center; margin: 0.5em 1em;">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <div style="margin: 0 1em;">{{$dataList->date}}</div>
                                    </div>
                                    <div style="display:flex; align-items: center;justify-content: space-between;">
                                        <div style="display:flex">
                                            <div style="margin-left:1em;">
                                            Total Shipment :<span class="text_processed" style="margin-left: 0.5em">{{$dataList->each_total_shipment}}</span>
                                            </div>

                                        </div>
                                        <button class="toggle_btn btn btn-link" data-toggle="collapse" data-target="#collapseOne{{ $loop->index }}" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        </button>
                                    </div>

                                </h5>
                                </div>

                                <div id="collapseOne{{ $loop->index }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="margin-bottom:20%">
                                @if(isset($dataList->data))
                                    @if(count($dataList->data)>0)
                                        @foreach($dataList->data as $d_detail)
                                        <div class="card-body" style="padding:0; margin-bottom:1em">
                                            <div class="collapse-cardbody-wrapper">
                                                <div class="collapse-cardbody-header">
                                                    <div class="header-left">
                                                        <div class="header-left-index">{{ $loop->index +1 }}</div>

                                                    </div>
                                                    <div class="header-right">

                                                    </div>
                                                </div>
                                                <div style="display:flex; justify-content:space-between">

                                                </div>

                                                <div class="collapse-cardbody-detail">
                                                    <div style="width:100%; font-size: small">

                                                        <div class="row" >
                                                            <div class="col-6 pr-0 d-grey">Ship Name </div>
                                                            <span class="col-2 pl-0 pr-1">{{$d_detail->ship_name}}</span>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-6 d-grey">Total Weight </div>
                                                            <span class="col-6 pl-0">
                                                                @if(!empty($d_detail->total_weight))
                                                                    {{$d_detail->total_weight}} MT
                                                                @endif
                                                            </span>
                                                        </div>
                                                        @if(isset($userLogged))
                                                            @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                                                            <div class="row">
                                                                <div style="white-space: nowrap;" class="col-6 d-grey">Total Weight (Price/MT)</div>
                                                                <span class="col-6 pl-0">
                                                                    @if(!empty($d_detail->total_weight_value))
                                                                        RM {{$d_detail->total_weight_value}}
                                                                    @endif
                                                                </span>
                                                            </div>
                                                            @endif
                                                        @endif

                                                        <div class="row">
                                                            <div class="col-6 d-grey">Draft Weight </div>
                                                            <span class="col-6 pl-0">
                                                                @if(!empty($d_detail->draft_weight))
                                                                    {{$d_detail->draft_weight}} MT
                                                                @endif
                                                            </span>
                                                        </div>

                                                        @if(isset($userLogged))
                                                            @if($userLogged->hasRole('admin') || $userLogged->hasRole('shareholder'))
                                                            <div class="row">
                                                                <div style="white-space: nowrap;" class="col-6 d-grey">Draft Weight (Price/MT)</div>
                                                                <span class="col-6 pl-0">
                                                                    @if(!empty($d_detail->draft_weight_value))
                                                                        RM {{$d_detail->draft_weight_value}}
                                                                    @endif
                                                                </span>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-6 d-grey">Purchase Value </div>
                                                                <span class="col-6 pl-0">RM {{$d_detail->purchase_value}}</span>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-6 d-grey">Sale </div>
                                                                <span class="col-6 pl-0">RM {{$d_detail->sale}}</span>
                                                            </div>
                                                            @endif
                                                        @endif

                                                        <div class="row">
                                                            <div class="col-6 d-grey">Moisture </div>
                                                            <span class="col-6 pl-0">{{$d_detail->moisture}} %</span>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-6 d-grey">Remark </div>
                                                            <span class="col-6 pl-0">{{$d_detail->remark}}</span>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="collapse-cardbody-doc">
                                                    <div style="width: 100%;">
                                                        <div class="row">
                                                            <div class="col-4 d-grey">Documents </div>
                                                            <div class="col-8 pl-0">
                                                            @if($d_detail->images)
                                                                @if(count($d_detail->images)>0)
                                                                    @foreach($d_detail->images as $img)
                                                                    <a target="_blank" href="{{asset('upload_shipment/'.$img->shipment_id.'/'.$img->image_url)}}">{{$img->caption ? $img->caption .'('. $img->mime_type.')':'No caption'.'('. $img->mime_type.')'}}</a>
                                                                        @if( !$loop->last)
                                                                            ,
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        @endforeach
                                    @endif
                                @else
                                    <div class="card-body" style="padding:0">
                                        <div class="collapse-cardbody-wrapper">
                                            <div class="collapse-cardbody-header">
                                                <div class="header-left">
                                                    <div>No Record for today</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                </div>
                            </div>
                            @endforeach
                        @endif
                        @endif

                    </div>

                    <!-- collapse -->

            </div>

        </div>

    </div>
</div>