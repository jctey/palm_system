@extends('layouts.app')

@section('content')

    @if($userLogged->hasRole('admin') || $userLogged->hasRole('supervisor')|| $userLogged->hasRole('clerk'))
    <div class="container" style="padding-bottom:5%">
        <div class="card">
            <div class="card-header" style="display:flex; justify-content:space-between">
                <div style="align-self: center;">Processed list</div>
                <div>
                    <a
                        class="btn btn-outline-primary"
                        style="box-shadow: 2px 3px 6px 4px #dddddd;"
                        href="{{ route('processed-create') }}"
                    >
                        Create
                    </a>
                    <button
                        type="button"
                        class="btn btn-outline-primary"
                        data-toggle="modal"
                        data-target="#processedFilterModalCenter"
                    >
                        <i class="fa fa-filter" aria-hidden="true"></i>
                    </button>
                </div>
            </div>

            <div class="card-body" style="overflow-x:auto">

                <div class="panel-body">
                    <table class="table table-bordered" id="processed-table" style="width:100%">
                        <thead>
                            <tr>
                                <th>Ticket No</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
    @else
    <div class="container" style="padding-bottom:5%">
        <div class="card">
            <div class="card-header" style="display:flex; justify-content:space-between">
                <div style="align-self: center;">You dont have permission</div>
            </div>
        </div>
    </div>


    @endif


@include('partials.bottom_navbar')
@include('partials.modal.confirm_modal')
@include('partials.modal.processed_filter_modal')

@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">

@endsection

@section('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>

<script>

        var table = $('#processed-table').DataTable({
        processing: true,
        serverSide: true,
        bLengthChange: true,
        ajax: {
            url:'{{ route('processed-list-datatable') }}',
            data: function (d) {
                console.log($('input[name="processed_status"]:checked').val())
                d.processed_status = $('input[name="processed_status"]:checked').val();

            }
        },

        columns: [
            // { data: 'DT_RowIndex', name: 'index',searchable: false },
            { data: 'ticket_no', name: 'ticket_no' },
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: false
            },
        ],
        order: [[1, 'asc']],
        "fnDrawCallback": function() {
            jQuery('#processed-table .toggleBtn').bootstrapToggle();
            rerender();
        }

        });

        $(document).ready(function () {

            $("#filter_btn").click(function(e) {
                table.ajax.reload();
            });

            $("#filter_reset_btn").click(function(e) {

                $('input[name="processed_status"]:checked').prop('checked', false);

                table.ajax.reload();
            });

            $('#confirmModal').on('shown.bs.modal', function(e) {

                //get data-id attribute of the clicked element

                let type = $(e.relatedTarget).data('modaltype');

                if(type == 'processed'){

                    let processed_id = $(e.relatedTarget).data('processedid');

                    let url = '{{ route("processed-delete", ":id") }}';
                    url = url.replace(':id', processed_id);
                    $('#confirm_delete_form').attr('action', url);
                }else return false


            });

        });
        function rerender(){
            $('.toggleBtn').change((e)=>{
                let checkedStatus = e.target.checked;
                let checkedId = e.target.getAttribute("data-id");

                $.ajax({
                    method: "POST",
                    url: "{{route('processed-update-toggle')}}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        id:checkedId,
                        status:checkedStatus
                    },
                    success: function (response) {
                        if(response.success == true) toastr.success(response.message);
                        if(response.success == false) toastr.error(response.message);
                        table.ajax.reload();

                    },
                });

            })
        }



</script>


@endsection