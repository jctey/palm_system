@extends('layouts.app')

@section('content')


    <div class="container" style="padding-bottom:5%">

        <div class="card">
        <div class="card-header" style="display:flex; justify-content:space-between">
            <div style="align-self: center;">Processed Details</div>
        </div>

        <div class="card-body">

            <div class="tabs_container">

                <div class="tab-wrap">

                <!-- active tab on page load gets checked attribute -->
                <input type="radio" id="tab1" name="tabGroup1" value="limited" class="tab">
                <label for="tab1">Processed Detail</label>

                @if(!$userLogged->hasRole('supervisor'))
                <input type="radio" id="tab2" name="tabGroup1" value="normal" class="tab">
                <label for="tab2">Document & Images Upload</label>
                @endif

                <div class="tab__content">
                    <div class="processed_detail_title">Incoming Transport</div>
                    <div class="form-group row">
                        <label for="ticket_no" class="col-md-4 col-form-label text-md-right">Ticket No</label>

                        <div class="col-md-6">
                            <input id="ticket_no" type="text" class="form-control @error('ticket_no') is-invalid @enderror" name="ticket_no" value="{{ $processed->ticket_no }}" readonly>
                        </div>
                    </div>
                    @if(!$userLogged->hasRole('supervisor'))
                    <div class="form-group row">
                        <label for="supplier" class="col-md-4 col-form-label text-md-right">Supplier</label>

                        <div class="col-md-6">
                            <input id="supplier" type="text" class="form-control @error('supplier') is-invalid @enderror" name="supplier" value="{{ $processed->supplier }}" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="transporter" class="col-md-4 col-form-label text-md-right">Transporter</label>

                        <div class="col-md-6">
                            <input id="transporter" type="text" class="form-control @error('transporter') is-invalid @enderror" name="transporter" value="{{ $processed->transporter }}" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Vehicle No</label>
                        @include('partials.processed.truckLoop')
                    </div>

                    <div class="form-group row">
                        <label for="product" class="col-md-4 col-form-label text-md-right">Product</label>

                        <div class="col-md-6">
                            <input id="product" type="text" class="form-control @error('product') is-invalid @enderror" name="product" value="{{ $processed->product }}" readonly>
                        </div>
                    </div>
                    @endif
                    <div class="form-group row">
                        <label for="weight" class="col-md-4 col-form-label text-md-right">Weight</label>

                        <div class="col-md-6" style="display:flex">
                            <input id="weight" type="text" style="border-radius:0.25em 0 0 0.25em" class="form-control @error('weight') is-invalid @enderror" name="weight" value="{{ $processed->weight }}" readonly>
                            <span class="input-suffix">MT</span>
                        </div>
                    </div>
                    @if(!$userLogged->hasRole('supervisor'))
                        @if($userLogged->hasRole('admin'))
                        <div class="form-group row">
                            <label for="transport" class="col-md-4 col-form-label text-md-right">Transport</label>

                            <div class="col-md-6" style="display:flex">
                                <span class="input-prefix">RM</span>
                                <input style="border-radius:0 0.25em 0.25em 0" id="transport" type="text" class="form-control @error('transport') is-invalid @enderror" name="transport" value="{{ $processed->transport }}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="goods_price" class="col-md-4 col-form-label text-md-right">Goods Price (Unit Price)</label>

                            <div class="col-md-6" style="display:flex">
                                <span class="input-prefix">RM</span>
                                <input style="border-radius:0 0.25em 0.25em 0" id="goods_price" type="text" class="form-control @error('goods_price') is-invalid @enderror" name="goods_price" value="{{ $processed->goods_price }}" readonly>
                            </div>
                        </div>
                        @endif

                    <div class="form-group row">
                        <label for="remark" class="col-md-4 col-form-label text-md-right">Remark</label>

                        <div class="col-md-6">
                            <textarea id="remark" class="form-control @error('remark') is-invalid @enderror" name="remark" value="{{ $processed->remark }}" readonly>{{ $processed->remark }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Issued by</label>
                        <div class="col-md-6">
                            <select name="issued_by" id="issued_by" class="form-control tags-selector" style="width:100%" disabled>
                                @if($adminSupervisorList->count() > 0)
                                    @foreach($adminSupervisorList as $adminSupervisor)
                                        <option value="{{$adminSupervisor->id}}"
                                            @if($processed->issued_by == $adminSupervisor->id)
                                                selected
                                            @endif
                                        >
                                            {{$adminSupervisor->name}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="processed_detail_title">Process (Moisture test)</div>

                    <div class="form-group row">
                        <label for="wastage" class="col-md-4 col-form-label text-md-right">Wastage</label>

                        <div class="col-md-6" style="display:flex">
                            <input id="wastage" style="border-radius:0.25em 0 0 0.25em" type="text" class="form-control @error('wastage') is-invalid @enderror" name="wastage" value="{{ $processed->wastage }}" readonly>
                            <span class="input-suffix">MT</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="moisture" class="col-md-4 col-form-label text-md-right">Moisture</label>

                        <div class="col-md-6" style="display:flex">
                            <input id="moisture" style="border-radius:0.25em 0 0 0.25em" type="text" class="form-control @error('moisture') is-invalid @enderror" name="moisture" value="{{ $processed->moisture }}" readonly>
                            <span class="input-suffix">%</span>
                        </div>
                    </div>

                    <input type="hidden" name="fileUploadArray">
                    <input type="hidden" name="fileUploadRemove">
                    @endif

                    @if($userLogged->hasRole('supervisor'))
                    <div class="form-group row">
                        <label for="processed_status" class="col-md-4 col-form-label text-md-right">Mark Processed Finish</label>
                        <div class="col-md-6">
                            <input class="toggleBtn" type="checkbox" id="processed_status" name="processed_status" value="finish" data-onstyle="success" data-offstyle="info" data-toggle="toggle" data-on="Yes" data-off="No" data-size="small"
                            @if($processed->processed_status == true)
                                checked disabled
                            @endif
                            >
                        </div>
                    </div>
                    @endif
                </div>
                <div class="tab__content">

                    <span class="text-danger" id="fileError"></span>
                    <div style="display:flex; flex-direction: column;">

                    <div style="overflow:auto">
                        <table class="table table-bordered mb-5">
                            <thead>
                                <tr class="table-success">
                                    <th scope="col">Image</th>

                                </tr>
                            </thead>
                            <tbody id="table_data">

                            </tbody>
                        </table>
                    </div>

                    </div>

                </div>

                </div>
            </div>

        </div>
        </div>
    </div>



@include('partials.bottom_navbar')
@include('partials.modal.create_vehicle_modal')
@include('partials.modal.dropzone_modal')
@include('partials.modal.image_caption_modal')

@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>


    <script>
        $(document).ready(function() {
            var elem =$('.tags-selector').select2()
            .on('select2:close', function() {
                var el = $(this);
                if(el.val()==="NEW") {

                    $("#createVehicleModal").modal()

                }
            });

            $("#tab1").prop("checked", true);

            $('#createVehicleModal').on('shown.bs.modal', function(e) {
                $('#new_vehicle_no').val('');
                    $('#vehicle_create').on('click',function(e){
                        e.preventDefault();

                        let newVehicleNo = $('#new_vehicle_no').val();
                        $.ajax({
                            url: "{{route('vehicle-store')}}",
                            type:"POST",
                            data:{
                                "_token": "{{ csrf_token() }}",
                                truck_plate_number:newVehicleNo,
                            },
                            success:function(response){
                                if(response.errors)
                                {
                                    $('.alert-danger').html('');

                                    $.each(response.errors, function(key, value){
                                        $('.alert-danger').show();
                                        $('.alert-danger').append('<li>'+value+'</li>');
                                    });
                                }
                                else
                                {
                                    $('.alert-danger').hide();
                                    $('#createVehicleModal').modal('hide');

                                }

                                if(response.success){
                                    generateTrucklist();
                                    // let truck = response.truck;
                                    // let html = `<option value="${truck.id}">${truck.truck_plate_number}</option>`;
                                    // elem.append(html).val(newVehicleNo);
                                }
                            },
                        });
                    });

            });
            function generateTrucklist(){
                $.ajax({
                    method: "GET",
                    url: "{{route('generate-truck-list')}}",
                    data: {

                    },
                    success: function (response) {
                        $('.collapse').collapse('show');
                        $('#truck').html(response)
                    },
                });
            }

            $('#imageCaptionModal').on('shown.bs.modal', function(e) {

            });

            $('.toggleBtn').change((e)=>{
                let checkedStatus = e.target.checked;
                let checkedId = "{{$processed->id}}";
                console.log(checkedStatus)
                if(checkedStatus == true){


                    $.ajax({
                        method: "POST",
                        url: "{{route('processed-update-toggle')}}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            id:checkedId,
                            status:checkedStatus
                        },
                        success: function (response) {
                            if(response.success == true) toastr.success(response.message);
                            if(response.success == false) toastr.error(response.message);
                            $('#processed_status').attr('disabled', true);
                        },
                    });


                }else {

                    toastr.error('Cannot unmark status');
                }


            })

        });

    </script>
    <script type="text/javascript">
        var fileArray = [];
        var fileArrayRemove = [];

        fileArray = {!! json_encode($arrayImageExist) !!}

        for(let i = 0; i < fileArray.length; i++){

            if(fileArray[i].mime == 'application/pdf'){
                var html = '<tr>';
                html += '<td><a target="_blank" href={{ URL::to('/') }}/upload/'+fileArray[i].folder_id+'/'+fileArray[i].path+'><label class="mr-2">'+fileArray[i].path+'</label><label class="imageTab_label ml-2" id="text_'+fileArray[i].name+'">'+fileArray[i].caption+'</label></a></td>';
            }else{
                var html = '<tr>';
                html += '<td><img width=200 height=150 src={{ URL::to('/') }}/upload/'+fileArray[i].folder_id+'/'+fileArray[i].path+'><label class="imageTab_label" id="text_'+fileArray[i].name+'">'+fileArray[i].caption+'</label></td></tr>';
            }

            $('#table_data').prepend(html);

        }

        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone",
            {
                maxFilesize: 10,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                return time+file.name;
                },
                autoProcessQueue: false,
                maxFiles: 1,
                init: function() {
                    this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                    });
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf",
                addRemoveLinks: true,
                timeout: 50000,
                removedfile: function(file)
                {
                    return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
                },
                success: function(file, response)
                {
                    this.removeFile(file);
                    $('#fileError').text('');

                    // store to hidden input
                    let name = file.upload.filename;

                    let obj = {};
                    obj["path"] = response.img_path;
                    obj["caption"] = '';
                    obj["ori_path"] = response.ori_path;
                    obj["mime"] = response.mime;
                    obj["name"] = response.name;

                    fileArray.push(obj);

                    $("input[name='fileUploadArray']").val(JSON.stringify(fileArray));

                    var html = '<tr>';
                    html += '<td><img width=200 height=150 src={{ URL::to('/') }}/upload/'+response.img_path+'><label class="imageTab_label" id="text_'+response.name+'"></label></td>';

                    $('#table_data').prepend(html);

                },
                error: function(file, response)
                {
                    if(response.errors) {
                        this.removeFile(file);
                        $('#fileError').text(response.errors.file);
                    }
                    return false;
                }
            });


// https://www.geeksforgeeks.org/how-to-dynamically-add-remove-table-rows-using-jquery/


    </script>
@endsection
