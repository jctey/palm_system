@extends('layouts.app')

@section('content')

    @if($userLogged->hasRole('admin') || $userLogged->hasRole('clerk'))
    <div class="container" style="padding-bottom:5%">
        <div class="card">
        <div class="card-header" style="display:flex; justify-content:space-between">
            <div style="align-self: center;">Create Incoming Processed</div>
        </div>

        <div class="card-body">

            <form id="processed_form" method="POST" action="{{ route('processed-store') }}" enctype="multipart/form-data">
                @csrf
            <div class="tabs_container">

                <div class="tab-wrap">

                <!-- active tab on page load gets checked attribute -->
                <input type="radio" id="tab1" name="tabGroup1" value="limited" class="tab">
                <label for="tab1">Processed Detail</label>

                <input type="radio" id="tab2" name="tabGroup1" value="normal" class="tab">
                <label for="tab2">Document & Images Upload</label>

                <div class="tab__content">
                    <div class="processed_detail_title">Incoming Transport</div>
                    <div class="form-group row">
                        <label for="ticket_no" class="col-md-4 col-form-label text-md-right">Ticket No</label>

                        <div class="col-md-6">
                            <input id="ticket_no" type="text" class="form-control @error('ticket_no') is-invalid @enderror" name="ticket_no" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('ticket_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="supplier" class="col-md-4 col-form-label text-md-right">Supplier</label>

                        <div class="col-md-6">
                            <input id="supplier" type="text" class="form-control @error('supplier') is-invalid @enderror" name="supplier" value="{{ old('supplier') }}">

                            @error('supplier')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="transporter" class="col-md-4 col-form-label text-md-right">Transporter</label>

                        <div class="col-md-6">
                            <input id="transporter" type="text" class="form-control @error('transporter') is-invalid @enderror" name="transporter" value="{{ old('transporter') }}">

                            @error('transporter')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Vehicle No</label>
                        @include('partials.processed.truckLoop')
                    </div>

                    <div class="form-group row">
                        <label for="product" class="col-md-4 col-form-label text-md-right">Product</label>

                        <div class="col-md-6">
                            <input id="product" type="text" class="form-control @error('product') is-invalid @enderror" name="product" value="{{ old('product') }}">

                            @error('product')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="weight" class="col-md-4 col-form-label text-md-right">Weight</label>

                        <div class="col-md-6" style="display:flex">
                            <input id="weight" style="border-radius:0.25em 0 0 0.25em" type="text" class="form-control @error('weight') is-invalid @enderror" name="weight" value="{{ old('weight') }}">
                            <span class="input-suffix">MT</span>
                            @error('weight')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    @if($userLogged->hasRole('admin'))
                    <div class="form-group row">
                        <label for="transport" class="col-md-4 col-form-label text-md-right">Transport</label>

                        <div class="col-md-6" style="display:flex">
                            <span class="input-prefix">RM</span>
                            <input style="border-radius:0 0.25em 0.25em 0" id="transport" type="text" class="form-control @error('transport') is-invalid @enderror" name="transport" value="{{ old('transport') }}">

                            @error('transport')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="goods_price" class="col-md-4 col-form-label text-md-right">Goods Price (Unit Price)</label>

                        <div class="col-md-6" style="display:flex">
                            <span class="input-prefix">RM</span>
                            <input style="border-radius:0 0.25em 0.25em 0" id="goods_price" type="text" class="form-control @error('goods_price') is-invalid @enderror" name="goods_price" value="{{ old('goods_price') }}">

                            @error('goods_price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    @endif

                    <div class="form-group row">
                        <label for="remark" class="col-md-4 col-form-label text-md-right">Remark</label>

                        <div class="col-md-6">
                            <textarea id="remark" class="form-control @error('remark') is-invalid @enderror" name="remark" value="{{ old('remark') }}"></textarea>

                            @error('remark')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Issued by</label>
                        <div class="col-md-6">
                            <select name="issued_by" id="issued_by" class="form-control tags-selector" style="width:100%"
                                @if(!$userLogged->hasRole('admin'))
                                    disabled
                                @endif
                            >
                                @if($adminSupervisorList->count() > 0)
                                    @foreach($adminSupervisorList as $adminSupervisor)
                                        <option value="{{$adminSupervisor->id}}"
                                            @if($userLogged->id == $adminSupervisor->id)
                                                selected
                                            @endif
                                        >
                                            {{$adminSupervisor->name}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            @error('issued_by')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="processed_detail_title">Process (Moisture test)</div>

                    <div class="form-group row">
                        <label for="wastage" class="col-md-4 col-form-label text-md-right">Wastage</label>

                        <div class="col-md-6" style="display:flex">
                            <input id="wastage" style="border-radius:0.25em 0 0 0.25em" type="text" class="form-control @error('wastage') is-invalid @enderror" name="wastage" value="{{ old('wastage') }}">
                            <span class="input-suffix">MT</span>
                            @error('wastage')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="moisture" class="col-md-4 col-form-label text-md-right">Moisture</label>

                        <div class="col-md-6" style="display:flex">
                            <input id="moisture" style="border-radius:0.25em 0 0 0.25em" type="text" class="form-control @error('moisture') is-invalid @enderror" name="moisture" value="{{ old('moisture') }}">
                            <span class="input-suffix">%</span>
                            @error('moisture')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    @if($userLogged->hasRole('clerk'))
                    <div class="form-group row">
                        <label for="created_at" class="col-md-4 col-form-label text-md-right">Created date</label>

                        <div class="col-md-6">
                            <input id="created_at" type="text" class="form-control @error('created_at') is-invalid @enderror" name="created_at" value="">

                            @error('created_at')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    @endif

                    @if($userLogged->hasRole('admin'))
                    <div class="form-group row">
                        <label for="processed_status" class="col-md-4 col-form-label text-md-right">Mark Processed Finish</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="processed_status" name="processed_status" value="finish" data-onstyle="success" data-offstyle="info" data-toggle="toggle" data-on="Yes" data-off="No" data-size="small">
                        </div>
                    </div>
                    @endif

                    <input type="hidden" name="fileUploadArray">
                </div>
                <div class="tab__content">

                    <span class="text-danger" id="fileError"></span>
                    <div style="display:flex; flex-direction: column;">

                    <div style="display:flex; flex-direction:row-reverse">

                        <button
                            type="button"
                            class="btn btn-outline-primary m-2"
                            data-toggle="modal"
                            data-target="#dropzoneModal"
                        >
                        Upload
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>

                    <div style="overflow:auto">
                        <table class="table table-bordered mb-5">
                            <thead>
                                <tr class="table-success">
                                    <th scope="col">Image</th>
                                    <th scope="col">Caption</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody id="table_data">

                            </tbody>
                        </table>
                    </div>

                    </div>

                </div>


                </div>
            </div>



                <div class="form-group row mb-0" style="justify-content: end; margin: 0 1%;">
                    <div style="text-align:center">
                        <button id="btn_submir_processed" type="button" class="btn btn-primary">
                            <span id="btn_create_text" class="" role="status" aria-hidden="true"></span>
                            Create
                            </button>
                        </button>
                    </div>
                </div>

            </form>



        </div>
        </div>
    </div>
    @else
        <div>You dont have permission</div>
    @endif

@include('partials.bottom_navbar')
@include('partials.modal.create_vehicle_modal')
@include('partials.modal.dropzone_modal')
@include('partials.modal.image_caption_modal')

@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
<meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>


    <script>
        $(document).ready(function() {

            flatpickr("#created_at",{
                enableTime:true,
                enableSeconds:true,
                dateFormat: "Y-m-d H:i:s",
                defaultDate: ["{{ date('Y-m-d') }} "]
            });

            var elem =$('.tags-selector').select2()
            .on('select2:close', function() {
                var el = $(this);
                if(el.val()==="NEW") {

                    $("#createVehicleModal").modal()

                }
            });

            $("#btn_submir_processed").click(()=>{
                $("#processed_form").submit();

                $("#btn_create_text").addClass('spinner-border spinner-border-sm');
                $('#btn_submir_processed').attr('disabled','disabled');

                $("#processed_form :input").prop('readonly', true);

            })

            $("#tab1").prop("checked", true);

            $('#createVehicleModal').on('shown.bs.modal', function(e) {
                $('#new_vehicle_no').val('');
                    $('#vehicle_create').on('click',function(e){
                        e.preventDefault();

                        let newVehicleNo = $('#new_vehicle_no').val();
                        $.ajax({
                            url: "{{route('vehicle-store')}}",
                            type:"POST",
                            data:{
                                "_token": "{{ csrf_token() }}",
                                truck_plate_number:newVehicleNo,
                            },
                            success:function(response){
                                if(response.errors)
                                {
                                    $('.alert-danger').html('');

                                    $.each(response.errors, function(key, value){
                                        $('.alert-danger').show();
                                        $('.alert-danger').append('<li>'+value+'</li>');
                                    });
                                }
                                else
                                {
                                    $('.alert-danger').hide();
                                    $('#createVehicleModal').modal('hide');

                                }

                                if(response.success){
                                    generateTrucklist();
                                    // let truck = response.truck;
                                    // let html = `<option value="${truck.id}">${truck.truck_plate_number}</option>`;
                                    // elem.append(html).val(newVehicleNo);
                                }
                            },
                        });
                    });

            });
            function generateTrucklist(){
                $.ajax({
                    method: "GET",
                    url: "{{route('generate-truck-list')}}",
                    data: {

                    },
                    success: function (response) {
                        $('.collapse').collapse('show');
                        $('#truck').html(response)
                    },
                });
            }

            $('#imageCaptionModal').on('shown.bs.modal', function(e) {

            });

        });

    </script>
    <script type="text/javascript">
        var fileArray = [];

        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone",
            {
                // 10 =10kb
                maxFilesize: 100,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                return time+file.name;
                },
                autoProcessQueue: false,
                maxFiles: 1,
                init: function() {
                    this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                    });
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif,.pdf",
                addRemoveLinks: true,
                timeout: 50000,
                removedfile: function(file)
                {
                    return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
                },
                success: function(file, response)
                {
                    this.removeFile(file);
                    $('#fileError').text('');

                    // store to hidden input
                    let name = file.upload.filename;

                    let obj = {};
                    obj["path"] = response.img_path;
                    obj["caption"] = '';
                    obj["ori_path"] = response.ori_path;
                    obj["mime"] = response.mime;
                    obj["data_name"] = response.name;

                    fileArray.push(obj);

                    $("input[name='fileUploadArray']").val(JSON.stringify(fileArray));
// URL.createObjectURL(imgurl)

                    if(response.mime == 'application/pdf'){
                        var html = '<tr>';
                        html += '<td><a target="_blank" href={{ URL::to('/') }}/upload/'+response.img_path+'><label class="mr-2">'+response.img_path+'</label><label class="imageTab_label ml-2" id="text_'+response.name+'"></label></a></td>';
                        html += '<td><button type="button" data-path='+response.img_path+' data-ori='+response.ori_path+' data-mime='+response.mime+' name="'+response.name+'" class="addCaption btn btn-primary">Add</button></td>';
                        html += '<td><button type="button" name='+response.img_path+' class="remove btn btn-danger">Delete</button></td></tr>';
                    }else{
                        var html = '<tr>';
                        html += '<td><img width=200 height=150 src={{ URL::to('/') }}/upload/'+response.img_path+'><label class="imageTab_label" id="text_'+response.name+'"></label></td>';
                        html += '<td><button type="button" data-path='+response.img_path+' data-ori='+response.ori_path+' data-mime='+response.mime+' name="'+response.name+'" class="addCaption btn btn-primary">Add</button></td>';
                        html += '<td><button type="button" name='+response.img_path+' class="remove btn btn-danger">Delete</button></td></tr>';
                    }

                    $('#table_data').prepend(html);

                },
                error: function(file, response)
                {
                    if(response.errors) {
                        this.removeFile(file);
                        $('#fileError').text(response.errors.file);
                    }
                    return false;
                }
            });

            $('#uploadFile').click(function(){
                myDropzone.processQueue();
                myDropzone.removeAllFiles();
            });

            $('#table_data').on('click', '.remove', function () {

                // Getting all the rows next to the
                // row containing the clicked button
                var child = $(this).closest('tr').nextAll();

                removeRow($(this).attr('name'))
                // Removing the current row.
                $(this).closest('tr').remove();

            });

            $('#table_data').on('click', '.addCaption', function () {

                var child = $(this).closest('tr').nextAll();
                let attName = $(this).attr('name');
                let attPath = $(this).attr('data-path');
                $('#imageCaptionModal').modal('show');
                $('#caption').val('');
                $('#caption').attr('name',attName);
                $('#caption').attr('data-path',attPath);

            });

            $('#addCaptionBtn').click(()=>{

                let captionContent = $('#caption').val();
                let captionText = $('#caption').attr('name');
                let path = $('#caption').attr('data-path');

                if(captionText != null && captionText !=''){

                    // captionContent = captionContent.replace(/\s/g, '');
                    let targetid = '#text_'+captionText;
                    $(targetid).text(captionContent);

                    fileArray.find(function(o){
                        if(o.path == path) return o.caption = captionContent;
                    })
                    $("input[name='fileUploadArray']").val(JSON.stringify(fileArray));
                    console.log(fileArray)
                }

            });

            function removeRow(imageName){

                var index = fileArray.findIndex(function(o){
                    return o.path == imageName;
                })
                if (index !== -1) fileArray.splice(index, 1);
                $("input[name='fileUploadArray']").val(JSON.stringify(fileArray));
                $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                },
                        type: 'POST',
                        url: '{{ route("image-delete") }}',
                        data: {filename: imageName},
                        success: function (data){
                            console.log("File has been successfully removed!!");
                        },
                        error: function(e) {
                            console.log(e);
                }});
            }


            $("#goods_price, #price_kg, #weight, #draft_weight, #total_weight, #wastage, #moisture, #transport").on("input", function(evt) {
                var self = $(this);
                self.val(self.val().replace(/[^0-9\.]/g, ''));
                if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
                {
                    evt.preventDefault();
                }
            });


// https://www.geeksforgeeks.org/how-to-dynamically-add-remove-table-rows-using-jquery/


    </script>
@endsection
