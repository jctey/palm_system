@extends('layouts.app')

@section('content')

    @if($userLogged->hasRole('admin'))
    <div class="container" style="padding-bottom:5%">
        <div class="card">
        <div class="card-header" style="display:flex; justify-content:space-between">
            <div style="align-self: center;">Create User</div>
        </div>

        <div class="card-body">

            <form id="processed_form" method="POST" action="{{ route('processed-store') }}" enctype="multipart/form-data">
                @csrf
            <div class="tabs_container">

                <div class="tab-wrap">

                <!-- active tab on page load gets checked attribute -->
                <input type="radio" id="tab1" name="tabGroup1" value="limited" class="tab">
                <label for="tab1">Processed Detail</label>

                <input type="radio" id="tab2" name="tabGroup1" value="normal" class="tab">
                <label for="tab2">Document & Images Upload</label>

                <div class="tab__content">
                    <div class="processed_detail_title">Incoming Transport</div>
                    <div class="form-group row">
                        <label for="ticket_no" class="col-md-4 col-form-label text-md-right">Ticket No</label>

                        <div class="col-md-6">
                            <input id="ticket_no" type="text" class="form-control @error('ticket_no') is-invalid @enderror" name="ticket_no" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('ticket_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="supplier" class="col-md-4 col-form-label text-md-right">Supplier</label>

                        <div class="col-md-6">
                            <input id="supplier" type="text" class="form-control @error('supplier') is-invalid @enderror" name="supplier" value="{{ old('supplier') }}">

                            @error('supplier')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="transporter" class="col-md-4 col-form-label text-md-right">Transporter</label>

                        <div class="col-md-6">
                            <input id="transporter" type="text" class="form-control @error('transporter') is-invalid @enderror" name="transporter" value="{{ old('transporter') }}">

                            @error('transporter')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Vehicle No</label>
                        @include('partials.processed.truckLoop')
                    </div>

                    <div class="form-group row">
                        <label for="product" class="col-md-4 col-form-label text-md-right">Product</label>

                        <div class="col-md-6">
                            <input id="product" type="text" class="form-control @error('product') is-invalid @enderror" name="product" value="{{ old('product') }}">

                            @error('product')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="weight" class="col-md-4 col-form-label text-md-right">Weight</label>

                        <div class="col-md-6">
                            <input id="weight" type="text" class="form-control @error('weight') is-invalid @enderror" name="weight" value="{{ old('weight') }}">

                            @error('weight')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="transport" class="col-md-4 col-form-label text-md-right">Transport</label>

                        <div class="col-md-6">
                            <input id="transport" type="text" class="form-control @error('transport') is-invalid @enderror" name="transport" value="{{ old('transport') }}">

                            @error('transport')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="wastage" class="col-md-4 col-form-label text-md-right">Wastage</label>

                        <div class="col-md-6">
                            <input id="wastage" type="text" class="form-control @error('wastage') is-invalid @enderror" name="wastage" value="{{ old('wastage') }}">

                            @error('wastage')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="remark" class="col-md-4 col-form-label text-md-right">Remark</label>

                        <div class="col-md-6">
                            <textarea id="remark" class="form-control @error('remark') is-invalid @enderror" name="remark" value="{{ old('remark') }}"></textarea>

                            @error('remark')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Issued by</label>
                        <div class="col-md-6">
                            <select name="issued_by" id="issued_by" class="form-control tags-selector" style="width:100%">
                                @if($adminSupervisorList->count() > 0)
                                    @foreach($adminSupervisorList as $adminSupervisor)
                                        <option value="{{$adminSupervisor->id}}"
                                            @if($userLogged->id == $adminSupervisor->id)
                                                selected
                                            @endif
                                        >
                                            {{$adminSupervisor->name}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            @error('issued_by')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="processed_detail_title">Process (Moisture test)</div>

                    <div class="form-group row">
                        <label for="moisture" class="col-md-4 col-form-label text-md-right">Moisture</label>

                        <div class="col-md-6">
                            <input id="moisture" type="text" class="form-control @error('moisture') is-invalid @enderror" name="moisture" value="{{ old('moisture') }}">

                            @error('moisture')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="processed_detail_title">Shipment</div>

                    <div class="form-group row">
                        <label for="ship_name" class="col-md-4 col-form-label text-md-right">Ship Name</label>

                        <div class="col-md-6">
                            <input id="ship_name" type="text" class="form-control @error('ship_name') is-invalid @enderror" name="ship_name" value="{{ old('ship_name') }}">

                            @error('ship_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="total_weight" class="col-md-4 col-form-label text-md-right">Total weight</label>

                        <div class="col-md-6">
                            <input id="total_weight" type="text" class="form-control @error('total_weight') is-invalid @enderror" name="total_weight" value="{{ old('total_weight') }}">

                            @error('total_weight')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="draft_weight" class="col-md-4 col-form-label text-md-right">Draft weight</label>

                        <div class="col-md-6">
                            <input id="draft_weight" type="text" class="form-control @error('draft_weight') is-invalid @enderror" name="draft_weight" value="{{ old('draft_weight') }}">

                            @error('draft_weight')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="processed_status" class="col-md-4 col-form-label text-md-right">Mark Processed Finish</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="processed_status" name="processed_status" value="finish" data-onstyle="success" data-offstyle="info" data-toggle="toggle" data-on="Yes" data-off="No" data-size="small">
                            <span style="margin-left:5px; font-size:small; color:red">* careful after mark finish cannot un-remark </span>
                        </div>
                    </div>
                </div>
                <div class="tab__content">

                    <div style="display:flex; flex-direction: column;">
                        <!-- <div class="input-group-btn">
                            <button id="addFile_btn" class="btn btn-success add-btn" type="button"><i class="fldemo fa fa-plus"></i></button>
                        </div> -->
                        <!-- <div class="input-group file-div control-group lst increment" style="display:none">
                            <input id="file_upload" type="file" name="files[]" class="myfrm form-control">
                        </div> -->
                        <!-- <div class="clone hide">
                            <div class="file-div control-group lst input-group" style="margin-top:10px">
                            <input type="file" name="files[]" class="myfrm form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-danger remove-btn" type="button"><i class="fldemo fa fa-close"></i></button>
                            </div>
                            </div>
                        </div> -->

                        <!-- https://www.nicesnippets.com/blog/laravel-8-multiple-image-upload-tutorial -->
                        <!-- https://laratutorials.com/laravel-8-upload-multiple-images-with-preview/ -->
                        <!-- https://www.pakainfo.com/multiple-image-upload-preview-remove-option/ -->
                        <label for="file" class="btn btn-success" style="height: 2.5em; width: 2.5em"><i class="fldemo fa fa-plus"></i></label>
                        <div class="member-upload">
                            <div class="member-edit">
                                <input type='file' id="file" name="files[]" multiple/>
                            </div>
                            <div class="member-preview">
                                <div class="imagePreview" style="display:none">
                                </div>
                            </div>
                        </div>

                        <button id="saveImg"> save</button>
                    </div>

                </div>


                </div>
            </div>



                <div class="form-group row mb-0" style="justify-content: end; margin: 0 1%;">
                    <div style="text-align:center">
                        <button id="btn_submir_processed" type="button" class="btn btn-primary">
                            <span id="btn_create_text" class="" role="status" aria-hidden="true"></span>
                            Create
                            </button>
                        </button>
                    </div>
                </div>
            </form>



        </div>
        </div>
    </div>
    @else
        <div>You dont have permission</div>
    @endif


@include('partials.bottom_navbar')
@include('partials.modal.create_vehicle_modal')
@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


    <script>
        $(document).ready(function() {
            var elem =$('.tags-selector').select2()
            .on('select2:close', function() {
                var el = $(this);
                if(el.val()==="NEW") {

                    $("#createVehicleModal").modal()


                // var newval = prompt("Enter new value: ");
                // if(newval !== null) {
                //     el.append('<option>'+newval+'</option>')
                //     .val(newval);
                // }
                }
            });

            $("#btn_submir_processed").click(()=>{
                $("#processed_form").submit();

                $("#btn_create_text").addClass('spinner-border spinner-border-sm');
                $('#btn_submir_processed').attr('disabled','disabled');

                $("#processed_form :input").prop('readonly', true);

            })


            $('.btn-password').click(()=>{
                var x = document.getElementById('password');
                if (x.type === 'password') {
                    x.type = "text";
                    $('#eyeShow').show();
                    $('#eyeSlash').hide();
                }else {
                    x.type = "password";
                    $('#eyeShow').hide();
                    $('#eyeSlash').show();
                }
            });

            $("#tab1").prop("checked", true);

            $('#createVehicleModal').on('shown.bs.modal', function(e) {
                $('#new_vehicle_no').val('');
                    $('#vehicle_create').on('click',function(e){
                        e.preventDefault();

                        let newVehicleNo = $('#new_vehicle_no').val();
                        $.ajax({
                            url: "{{route('vehicle-store')}}",
                            type:"POST",
                            data:{
                                "_token": "{{ csrf_token() }}",
                                truck_plate_number:newVehicleNo,
                            },
                            success:function(response){
                                if(response.errors)
                                {
                                    $('.alert-danger').html('');

                                    $.each(response.errors, function(key, value){
                                        $('.alert-danger').show();
                                        $('.alert-danger').append('<li>'+value+'</li>');
                                    });
                                }
                                else
                                {
                                    $('.alert-danger').hide();
                                    $('#createVehicleModal').modal('hide');

                                }

                                if(response.success){
                                    generateTrucklist();
                                    // let truck = response.truck;
                                    // let html = `<option value="${truck.id}">${truck.truck_plate_number}</option>`;
                                    // elem.append(html).val(newVehicleNo);
                                }
                            },
                        });
                    });

            });
            function generateTrucklist(){
                $.ajax({
                    method: "GET",
                    url: "{{route('generate-truck-list')}}",
                    data: {

                    },
                    success: function (response) {
                        $('.collapse').collapse('show');
                        $('#truck').html(response)
                    },
                });
            }

            // $('#addFile_btn').click(()=>{
            //     $('#file_upload').click();
            // })

            var fileList=[];

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var filesAmount = input.files.length;

                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            var d = $('.imagePreview:first').clone()
                            d.css('background-image', 'url('+e.target.result +')');
                            d.html('<span class="img-delete"><b class="remove_icon">X</b></span>');
                            d.hide();
                            d.fadeIn(650);
                            $('.member-preview').append($(d));
                            $(".img-delete").click(function(){
                                $(this).parent(".imagePreview").remove();
                            });
                        }

                        reader.readAsDataURL(input.files[i]);
                        fileList.push(input.files[i]);

                    }

                }
            }
            $("#file").change(function() {
                readURL(this);
            });
            $('#saveImg').click((e)=>{
                e.preventDefault();

                fileList.forEach((file)=>{
                    console.log(file)
                })
            })

            $("processed_form").submit(function(e) {
                e.preventDefault();
                console.log('submit')

            });


        });

    </script>
@endsection
