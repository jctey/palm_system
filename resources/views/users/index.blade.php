@extends('layouts.app')

@section('content')

    @if($userLogged->hasRole('admin'))
    <div class="container" style="padding-bottom:5%">
        <div class="card">
            <div class="card-header" style="display:flex; justify-content:space-between">
                <div style="align-self: center;">User list</div>
                <div>
                    <a
                        class="btn btn-outline-primary"
                        style="box-shadow: 2px 3px 6px 4px #dddddd;"
                        href="{{ route('user-create') }}"
                    >
                        Create Newuser
                    </a>
                    <button
                        type="button"
                        class="btn btn-outline-primary"
                        data-toggle="modal"
                        data-target="#userFilterModalCenter"
                    >
                        <i class="fa fa-filter" aria-hidden="true"></i>
                    </button>
                </div>
            </div>

            <div class="card-body" style="overflow-x:auto">

                <div class="panel-body">
                    <table class="table table-bordered" id="users-table" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>

                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
    @else
    <div class="container" style="padding-bottom:5%">
        <div class="card">
            <div class="card-header" style="display:flex; justify-content:space-between">
                <div style="align-self: center;">You dont have permission</div>
            </div>
        </div>
    </div>


    @endif


@include('partials.bottom_navbar')
@include('partials.modal.confirm_modal')
@include('partials.modal.user_filter_modal')

@endsection

@section('head')
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>

<script>

        var table = $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        bLengthChange: true,
        ajax: {
            url:'{{ route('user-list-datatable') }}',
            data: function (d) {
                d.role = $('input[name="role"]:checked').val();
                d.verify = $('input[name="verify"]:checked').val();
                d.user_verified = $('input[name="user_verified"]:checked').val();
            }
        },

        columns: [
            { data: 'DT_RowIndex', name: 'index',searchable: false },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: false
            },
        ],
        order: [[1, 'asc']]
        });

        $(document).ready(function () {

            $("#filter_btn").click(function(e) {
                table.ajax.reload();
            });

            $("#filter_reset_btn").click(function(e) {

                $('input[name="verify"]:checked').prop('checked', false);
                $('input[name="role"]:checked').prop('checked', false);
                $('input[name="user_verified"]:checked').prop('checked', false);

                table.ajax.reload();
            });

            $('#confirmModal').on('shown.bs.modal', function(e) {

                //get data-id attribute of the clicked element

                let type = $(e.relatedTarget).data('modaltype');

                if(type == 'user'){

                    let user_id = $(e.relatedTarget).data('userid');
                    let user_email = $(e.relatedTarget).data('useremail');

                    let url = '{{ route("user-delete", ":id") }}';
                    url = url.replace(':id', user_id);
                    $('#confirm_delete_form').attr('action', url);
                }else return false


            });


        });



</script>


@endsection