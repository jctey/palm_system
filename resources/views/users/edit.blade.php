@extends('layouts.app')

@section('content')

    @if($userLogged->hasRole('admin'))
    <div class="container" style="padding-bottom:5%">
        <div class="card">
        <div class="card-header" style="display:flex; justify-content:space-between">
            <div style="align-self: center;">Edit User</div>
        </div>
        @if(isset($user))
        <div class="card-body">

            <form method="POST" action="{{ route('user-update',$user->id) }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-5">
                        <input id="password" placeholder="Remain password leave it empty" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-1">
                        <span class="input-group-btn" id="eyeSlash">
                            <button class="btn-password btn btn-outline-dark btn-sm reveal" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                        </span>
                        <span class="input-group-btn" id="eyeShow" style="display: none;">
                            <button class="btn-password btn btn-outline-dark btn-sm reveal" type="button"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        </span>
                    </div>
                </div>

                @if($roles->count() > 0)
                <div class="form-group row">
                    <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                    <div class="col-md-6">
                        <select name="role[]" id="role" class="form-control tags-selector">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}"
                                    @if(isset($role_id))
                                        @if($role_id == $role->id)
                                            selected
                                        @endif
                                    @endif
                                >
                                    {{$role->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif

                @if(!$user->hasVerifiedEmail())
                <div class="form-group row">
                    <label for="verify" class="col-md-4 col-form-label text-md-right">Email Verified</label>
                    <div class="col-md-6">
                        <input type="checkbox" id="verify" name="verify" value="verify" data-onstyle="success" data-offstyle="info" data-toggle="toggle" data-on="Yes" data-off="No" data-size="small">

                    </div>
                </div>
                @endif

                <div class="form-group row">
                    <label for="user_verified" class="col-md-4 col-form-label text-md-right">Mark user verified</label>
                    <div class="col-md-6">
                        <input type="checkbox" id="user_verified" name="user_verified" value="user_verified" data-onstyle="success" data-offstyle="info" data-toggle="toggle" data-on="Yes" data-off="No" data-size="small"
                        @if($user->user_verified == true)
                            checked
                        @endif
                        >
                    </div>
                </div>


                <div class="form-group row mb-0" style="justify-content: end; margin: 0 1%;">
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>
            </form>



        </div>
        @endif
        </div>
    </div>
    @else
        <div>You dont have permission</div>
    @endif


@include('partials.bottom_navbar')

@endsection

@section('head')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


    <script>
        $(document).ready(function() {
            $('.tags-selector').select2();
        });


        $('.btn-password').click(()=>{
            var x = document.getElementById('password');
            if (x.type === 'password') {
                x.type = "text";
                $('#eyeShow').show();
                $('#eyeSlash').hide();
            }else {
                x.type = "password";
                $('#eyeShow').hide();
                $('#eyeSlash').show();
            }
        });


    </script>
@endsection
