@extends('layouts.app')

@section('content')
<div class="container" style="padding-bottom:5%">
    <div class="row justify-content-center" >
        <div style="background-color:#2538a3; width: 100%">

            <div class="dashboard_wrapper_box_m">
                @if($user->user_verified == true)
                <div class="dpicker_mobile_outer_wrap">
                    <div class="dpicker_mobile_wrap">
                        <button id="btn_filter_dashboard" class="btn_filter_mobile">Apply Filter</button>
                        <input id="dpicker_mobile" type="text" class="form-control form-control-sm" value=""
                        data-type="daterange-picker-mobile" placeholder="select date" style="display:none">
                        <div style="display:flex; width:80%">

                            <div id="text_range_selected" style="font-weight: bold; width:100%"></div>
                        </div>

                    </div>
                </div>
                    <div id="rerender_view">
                        @include('partials.mobile_dashboard')
                    </div>
                @else
                <div> Your Account are not yet verified by admin </div>
                @endif
            </div>

        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
$( document ).ready(function() {
    const $daterangePicker = $('[data-type="daterange-picker-mobile"]');
    var DateSelected;

    initFlatPicker();

    function initFlatPicker(){
        // today
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        today = yyyy + '-' + mm + '-' +dd;

        if ($daterangePicker.length > 0) {
            $daterangePicker.flatpickr({
                mode: 'range',
                inline: true,
                dateFormat: "Y-m-d",
                defaultDate: today+" to "+today,
                onChange: function(selectedDates, dateStr, instance) {

                    //dateStr = dateStr.replace('to','~')
                    dateStr = dateStr.split('to');
                    console.log(dateStr)
                    //$('#text_range_selected').text(dateStr);

                    if(dateStr.length == 2){
                        let html = '<div style="display:flex;width: 100%; justify-content: space-around;">';
                        html += '<div style="display:flex; text-align:center"><div><div class="d-grey">Start Date</div><div>'+dateStr[0]+'</div></div><i style="align-self: center" class="fa fa-calendar m-2" aria-hidden="true"></i></div>';
                        html += '<div style="display:flex; text-align:center"><div><div class="d-grey">End Date</div><div>'+dateStr[1]+'</div></div><i style="align-self: center" class="fa fa-calendar m-2" aria-hidden="true"></i></div>';
                        html += '</div>';
                        $('#text_range_selected').html(html);
                    }else{
                        let html = '<div style="display:flex;width: 100%; justify-content: space-around;">';
                        html += '<div style="display:flex; text-align:center"><div><div class="d-grey">Single Date</div><div>'+dateStr[0]+'</div></div><i style="align-self: center" class="fa fa-calendar m-2" aria-hidden="true"></i></div>';
                        html += '</div>';
                        $('#text_range_selected').html(html);
                    }


                },

            });
        }
        filterSubmit();
        $('#text_range_selected').text(today);
        let html = '<div style="display:flex;width: 100%; justify-content: space-around;">';
        html += '<div style="display:flex; text-align:center"><div><div class="d-grey">Single Date</div><div>'+today+'</div></div><i style="align-self: center" class="fa fa-calendar m-2" aria-hidden="true"></i></div>';
        html += '</div>';
        $('#text_range_selected').html(html);
    }

    function filterSubmit(){

        let dateSelected = $('#dpicker_mobile').val();
        dateSelected = dateSelected.replace(/\s+/g, '')
        let dateSelectedArr = dateSelected.split('to');

        if(dateSelectedArr == undefined) alert('Please select date before filter');
        else{

            $.ajax({
                method: "GET",
                url: "{{route('filter-mobile')}}",
                data: {
                    date:dateSelectedArr
                },
                success: function (response) {
                    // $('.collapse').collapse('show');
                    $('#rerender_view').html(response)
                    rerenderButton();
                },
            });

        }
    }


    $('#btn_clear_filter_dashboard').click((e)=>{

        $daterangePicker.flatpickr().clear();
        initFlatPicker();
        DateSelected=[];
        e.preventDefault();
        filterSubmit()

    })

    rerenderButton();

    function rerenderButton(){

        detectSpanLength();

        $('#btn_to_shipment_mobile').on('click', function(){
            $('#incoming_wrapper_mobile').addClass('d-none');
            $('#shipment_wrapper_mobile').removeClass('d-none');

            let obj = document.getElementById("total_shipment");
            let objvalue = $('#total_shipment').text();

            animateValue(obj, 0, objvalue, 1500);
        });
        $('#btn_to_incoming_mobile').on('click', function(){
            $('#incoming_wrapper_mobile').removeClass('d-none');
            $('#shipment_wrapper_mobile').addClass('d-none');
        });

        $('#editIconShipment, #editIconProcessed').click((e)=>{
            e.preventDefault();
            e.stopPropagation();
            showMobileCalendar();
        })

        $('.dpicker_mobile_outer_wrap').click((e)=>{
            e.preventDefault();
            e.stopPropagation();
            hideMobileCalendar();
        })

        $('.dpicker_mobile_wrap').click((e)=>{
            e.preventDefault();
            e.stopPropagation();
            // prevent click div inside trigger close calander
        })

        $('#btn_filter_dashboard').unbind().click((e)=>{
            e.preventDefault();
            hideMobileCalendar();
            filterSubmit();
        })

        $('.see_details').click((e)=>{
            e.preventDefault();
            e.stopPropagation();
            $('#statistic_m').addClass('d-none');
            $('#details_m').removeClass('d-none');
            $('#statistic_shipment_m').addClass('d-none');
            $('#details_shipment_m').removeClass('d-none');
            $('#btn_to_shipment_mobile').addClass('d-none');
            $('#btn_to_incoming_mobile').addClass('d-none');
        })

        $('.go_back').click((e)=>{
            e.preventDefault();
            e.stopPropagation();
            $('#details_m').addClass('d-none');
            $('#statistic_m').removeClass('d-none');
            $('#details_shipment_m').addClass('d-none');
            $('#statistic_shipment_m').removeClass('d-none');
            $('#btn_to_shipment_mobile').removeClass('d-none');
            $('#btn_to_incoming_mobile').removeClass('d-none');
        })

        $('.toggle_btn').on('click', function() {
            //$(this).toggleClass('fa-chevron-down fa-chevron-up');
            if($(this).find('i').hasClass("fa fa-chevron-up")){
                $(this).find('i').removeClass("fa fa-chevron-up").addClass("fa fa-chevron-down")
            }else{
                $(this).find('i').removeClass("fa fa-chevron-down").addClass("fa fa-chevron-up")
            }
        });

        // loading animation  ---value *0100
        let processed_loadingbar_value = $('#progress-bar-processed').attr('data-totalP');
        let unprocessed_loadingbar_value = $('#progress-bar-unprocessed').attr('data-totalU');
        LoadingBarAnimate('#progress-bar-processed',processed_loadingbar_value);
        LoadingBarAnimate('#progress-bar-unprocessed',unprocessed_loadingbar_value);

    }

    function showMobileCalendar(){
        $('.dpicker_mobile_outer_wrap').css('display','flex');
        $('#rerender_view').css('display','none');
    }

    function hideMobileCalendar(){
        $('.dpicker_mobile_outer_wrap').css('display','none');
        $('#rerender_view').css('display','block');
    }


    function LoadingBarAnimate(targetId,value){
        let max = $(targetId).attr('aria-valuemax');
        $(""+targetId+"-text").text(value);

        value = ((value / max) *100).toFixed(2);;

        let percent = ""+value+"%";
        $(targetId).animate({width: percent}, 3000);


    }
    function animateValue(obj, start, end, duration) {

        if(end != null && end != undefined && end != ''){
            let startTimestamp = null;
            const step = (timestamp) => {
                if (!startTimestamp) startTimestamp = timestamp;
                const progress = Math.min((timestamp - startTimestamp) / duration, 1);
                obj.innerHTML = Math.floor(progress * (end - start) + start);
                if (progress < 1) {
                window.requestAnimationFrame(step);
                }
            };
                window.requestAnimationFrame(step);
        }

    }

    function detectSpanLength(){

        if($('#total_purchase_m').text().length > 10) $('#total_purchase_m').css('font-size','x-large');

        if($('#total_sale_m').text().length > 10) $('#total_sale_m').css('font-size','large');

        if($('#total_weight_in_stock_m').text().length > 11) $('#total_weight_in_stock_m').css('font-size','large');

        if($('#total_value_m').text().length > 11) $('#total_value_m').css('font-size','large');

        if($('#total_ready_stock_m').text().length > 11) $('#total_ready_stock_m').css('font-size','large');

    }

});
</script>
@endsection