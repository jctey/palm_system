@extends('layouts.app')

@section('content')

    @if($userLogged->hasRole('admin') || $userLogged->hasRole('clerk'))
    <div class="container" style="padding-bottom:5%">
        <div class="card">
            <div class="card-header" style="display:flex; justify-content:space-between">
                <div style="align-self: center;">Vehicle list</div>
            </div>

            <div class="card-body" style="overflow-x:auto">

                <div class="panel-body">
                    <table class="table table-bordered" id="truck-table" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Vehicle Number</th>
                                <th>Count of Used</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>
    @else
    <div class="container" style="padding-bottom:5%">
        <div class="card">
            <div class="card-header" style="display:flex; justify-content:space-between">
                <div style="align-self: center;">You dont have permission</div>
            </div>
        </div>
    </div>


    @endif


@include('partials.bottom_navbar')
@include('partials.modal.confirm_modal')

@endsection

@section('head')
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.min.js"></script>

<script>

        var table = $('#truck-table').DataTable({
        processing: true,
        serverSide: true,
        bLengthChange: true,
        ajax: {
            url:'{{ route('vehicle-list-datatable') }}',
            data: function (d) {

            }
        },

        columns: [
            { data: 'DT_RowIndex', name: 'index',searchable: false },
            { data: 'truck_plate_number', name: 'truck_plate_number' },
            { data: 'count', name: 'count' ,searchable: false},
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: false
            },
        ],
        order: [[1, 'asc']]
        });

        $(document).ready(function () {

            $('#confirmModal').on('shown.bs.modal', function(e) {

                //get data-id attribute of the clicked element

                let type = $(e.relatedTarget).data('modaltype');

                if(type == 'truck'){

                    let truck_id = $(e.relatedTarget).data('truckid');

                    let url = '{{ route("vehicle-delete", ":id") }}';
                    url = url.replace(':id', truck_id);
                    $('#confirm_delete_form').attr('action', url);
                }else return false


            });


        });



</script>


@endsection