@extends('layouts.app')

@section('content')
<div class="container" style="padding-bottom:5%">
    <div class="row justify-content-center">
        <div class="col-md-12">
        @if($user->hasRole('admin'))
           <p>welcome admin</p>
        @elseif($user->hasRole('supervisor'))
            <p>welcome supervisor</p>
        @elseif($user->hasRole('shareholder'))
            <p>welcome shareholder</p>
        @endif

            <div class="card">
            <div class="dashboard_wrapper">
                @if($user->user_verified == true)
                <div class="dahsboard_col_item">
                    <button type="button" class="btn btn-outline-primary" onclick="window.location='{{ route("dashboard") }}'">
                        <i class="fa fa-television fa-2x" aria-hidden="true"></i>
                        <strong>Dashboard</strong>
                    </button>
                </div>
                <div class="dahsboard_col_item">
                    <button type="button" class="btn btn-outline-secondary" onclick="window.location='{{ route("users") }}'">
                        <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                        <strong>User list</strong>
                    </button>
                </div>
                <div class="dahsboard_col_item">
                    <button type="button" class="btn btn-outline-success" onclick="window.location='{{ route("processeds") }}'">
                        <i class="fa fa-list fa-2x" aria-hidden="true"></i>
                        <strong>Processed list</strong>
                    </button>
                </div>

                <div class="dahsboard_col_item">
                    <button type="button" class="btn btn-outline-warning" onclick="window.location='{{ route("shipments") }}'">
                        <i class="fa fa-exchange fa-2x" aria-hidden="true"></i>
                        <strong>Shipment list</strong>
                    </button>
                </div>

                <div class="dahsboard_col_item">
                    <button type="button" class="btn btn-outline-danger" onclick="window.location='{{ route("vehicles") }}'">
                        <i class="fa fa-truck fa-2x" aria-hidden="true"></i>
                        <strong>Vehicle list</strong>
                    </button>
                </div>

                @else
                <div> Your Account are not yet verified by admin </div>
                @endif
            </div>
            </div>

        </div>
    </div>
</div>

@endsection
