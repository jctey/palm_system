<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProcessedController;
use App\Http\Controllers\TruckController;
use App\Http\Controllers\ShipmentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true,'register' => false]);

Route::group(['middleware' => ['auth','verified']], function(){

    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
    Route::get('/filter', [HomeController::class, 'filter'])->name('filter');
    Route::get('/filter-mobile', [HomeController::class, 'filterMobile'])->name('filter-mobile');

    Route::get('/users', [UserController::class, 'index'])->name('users');
    Route::get('/user/create', [UserController::class, 'create'])->name('user-create');
    Route::post('/user/store', [UserController::class, 'store'])->name('user-store');
    Route::get('/user/edit/{id}', [UserController::class, 'edit'])->name('user-edit');
    Route::post('/user/update/{id}', [UserController::class, 'update'])->name('user-update');
    Route::delete('/user/delete/{id}', [UserController::class, 'delete'])->name('user-delete');
    Route::get('/users-list-datatable', [UserController::class, 'userListDatatable'])->name('user-list-datatable');

    Route::get('/processed-list', [ProcessedController::class, 'index'])->name('processeds');
    Route::get('/processed/create', [ProcessedController::class, 'create'])->name('processed-create');
    Route::post('/processed/store', [ProcessedController::class, 'store'])->name('processed-store');
    Route::get('/processed/view/{id}', [ProcessedController::class, 'view'])->name('processed-view');
    Route::get('/processed/edit/{id}', [ProcessedController::class, 'edit'])->name('processed-edit');
    Route::post('/processed/update/{id}', [ProcessedController::class, 'update'])->name('processed-update');
    Route::post('/processed/toggle-update', [ProcessedController::class, 'updateToggle'])->name('processed-update-toggle');
    Route::delete('/processed/delete/{id}', [ProcessedController::class, 'delete'])->name('processed-delete');
    Route::get('/processed-list-datatable', [ProcessedController::class, 'listDatatable'])->name('processed-list-datatable');
    Route::post('/image/store', [ProcessedController::class, 'uploadImages'])->name('image-store');
    Route::post('/image/delete', [ProcessedController::class, 'deleteImage'])->name('image-delete');

    Route::get('/shipment-list', [ShipmentController::class, 'index'])->name('shipments');
    Route::get('/shipment-list-datatable', [ShipmentController::class, 'listDatatable'])->name('shipment-list-datatable');
    Route::get('/shipment/create', [ShipmentController::class, 'create'])->name('shipment-create');
    Route::post('/shipment/store', [ShipmentController::class, 'store'])->name('shipment-store');
    Route::get('/shipment/edit/{id}', [ShipmentController::class, 'edit'])->name('shipment-edit');
    Route::post('/shipment/update/{id}', [ShipmentController::class, 'update'])->name('shipment-update');
    Route::post('/shipment/image/store', [ShipmentController::class, 'uploadImages'])->name('shipment-image-store');
    Route::post('/shipment/image/delete', [ShipmentController::class, 'deleteImage'])->name('shipment-image-delete');
    Route::delete('/shipment/delete/{id}', [ShipmentController::class, 'delete'])->name('shipment-delete');

    Route::get('/vehicle-list', [TruckController::class, 'index'])->name('vehicles');
    Route::post('/vehicle/store', [TruckController::class, 'store'])->name('vehicle-store');
    Route::get('/generateTruckList', [TruckController::class, 'generateList'])->name('generate-truck-list');
    Route::get('/vehicle-list-datatable', [TruckController::class, 'listDatatable'])->name('vehicle-list-datatable');
    Route::delete('/vehicle/delete/{id}', [TruckController::class, 'delete'])->name('vehicle-delete');
});

