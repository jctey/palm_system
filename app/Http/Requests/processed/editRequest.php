<?php

namespace App\Http\Requests\processed;

use Illuminate\Foundation\Http\FormRequest;

class editRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('processed_status') == 'finish') {
            return [
                'ticket_no' => 'required|string|max:10|unique:processeds,ticket_no,'.$this->id,
                'weight' => 'required|numeric',
            ];
        }else{
            return [
                'ticket_no' => 'required|string|max:10|unique:processeds,ticket_no,'.$this->id,
            ];
        }
    }
}
