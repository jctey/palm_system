<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\User;
use App\Models\Role;
use App\Models\Trucks;
use App\Models\Processed;
use App\Models\Processed_Images;
use Carbon\Carbon;
use Session;
use App\Http\Requests\processed\createRequest;
use App\Http\Requests\processed\editRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use File;

class ProcessedController extends Controller
{
    private $userLogged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->userLogged = auth()->user();

        return view('processed.index')
                ->with('userLogged',$this->userLogged);
    }

    public function listDatatable(Request $req)
    {
        $this->userLogged = auth()->user();

        $processeds = Processed::select(['id','moisture','ticket_no', 'processed_status','created_at'])->orderBy('created_at','ASC');

        if($req->processed_status){

            if($req->processed_status == 'processed') $processeds = $processeds->where('processed_status',1);
            else $processeds = $processeds->where('processed_status',0);
        }

        return Datatables::of($processeds)
            ->addIndexColumn()
            ->addColumn('action', function ($processed) {

                // return '<div style="display:flex; justify-content: space-evenly;">
                //         <input type="checkbox" class="toggleBtn" id="processed_status_'.$processed->id.'" data-id="'.$processed->id.'" name="processed_status_toggle" value="finish" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Accept" data-off="Decline" data-size="small"
                //         '. $checked .'
                //         >
                //         <a href="'.route('processed-view',$processed->id).'" class="view btn btn-success btn-sm shadow"><i class="fa fa-eye" aria-hidden="true"></i></a>
                //         <a href="'.route('processed-edit',$processed->id).'" class="edit btn btn-info btn-sm shadow"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                //         <button data-toggle="modal" data-target="#confirmModal" data-modaltype="processed"  data-processedid='.$processed->id.' class="delete btn btn-danger btn-sm shadow"><i class="fa fa-trash" aria-hidden="true"></i></button>
                //         </div';
                if($this->userLogged->hasRole('supervisor')){
                    $checked = ($processed->processed_status == 1) ? 'checked' : '';
                    return '<div style="display:flex; justify-content: space-evenly;">
                        <a href="'.route('processed-view',$processed->id).'" class="view btn btn-success btn-sm shadow"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a href="'.route('processed-edit',$processed->id).'" class="edit btn btn-info btn-sm shadow"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <button data-toggle="modal" data-target="#confirmModal" data-modaltype="processed"  data-processedid='.$processed->id.' class="delete btn btn-danger btn-sm shadow"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </div';
                }else{
                    return '<div style="display:flex; justify-content: space-evenly;">
                        <a href="'.route('processed-view',$processed->id).'" class="view btn btn-success btn-sm shadow"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a href="'.route('processed-edit',$processed->id).'" class="edit btn btn-info btn-sm shadow"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        <button data-toggle="modal" data-target="#confirmModal" data-modaltype="processed"  data-processedid='.$processed->id.' class="delete btn btn-danger btn-sm shadow"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </div';
                }

            })
            ->editColumn('ticket_no', function ($t) {
                if($t->processed_status == true){
                    return '<div class="status_processed_ticket_no">'.$t->ticket_no.'</div>';
                }else{
                    return '<div class="status_unprocessed_ticket_no">'.$t->ticket_no.'</div>';
                }

            })
            ->editColumn('id', '{{$id}}')
            ->rawColumns(['action','ticket_no'])
            ->setRowClass(function ($processed) {
                return $processed->moisture >= 30 ? 'alert-warning' : 'alert-light';
            })
            ->make(true);
    }

    public function view($id)
    {
        $this->userLogged = auth()->user();

        $rolelist = Role::get();
        $users_roles = DB::table('users_roles')
                    ->where('role_id',1)
                    ->orWhere('role_id',2)
                    ->orWhere('role_id',3)
                    ->orWhere('role_id',4)
                    ->pluck('user_id');
        $adminSupervisorList = User::whereIn('id',$users_roles)->get();

        $images = Processed_Images::where('processed_id',$id)->get();

        $arrayImageExisting = [];
        $arrayImageExisting = $images->map(function($item, $key) {
            return [
                'caption' => $item['caption'],
                'mime' => $item['mime_type'],
                'ori_path' => $item['image_ori_name'],
                'path' => $item['image_url'],
                'name' => $item['data_name'],
                'folder_id' => $item['processed_id']
            ];
        });

        return view('processed.view')
                ->with('userLogged',$this->userLogged)
                ->with('processed',Processed::findOrFail($id))
                ->with('trucks',Trucks::get())
                ->with('adminSupervisorList',$adminSupervisorList)
                ->with('images',$images)
                ->with('arrayImageExist',$arrayImageExisting)
                ->with('isViewPage',true);

    }

    public function edit($id)
    {
        $this->userLogged = auth()->user();
        if($this->userLogged->hasRole('admin') || $this->userLogged->hasRole('clerk')){
            $rolelist = Role::get();
            $users_roles = DB::table('users_roles')
                        ->where('role_id',1)
                        ->orWhere('role_id',2)
                        ->orWhere('role_id',3)
                        ->orWhere('role_id',4)
                        ->pluck('user_id');
            $adminSupervisorList = User::whereIn('id',$users_roles)->get();

            $images = Processed_Images::where('processed_id',$id)->get();

            $arrayImageExisting = [];
            $arrayImageExisting = $images->map(function($item, $key) {
                return [
                    'caption' => $item['caption'],
                    'mime' => $item['mime_type'],
                    'ori_path' => $item['image_ori_name'],
                    'path' => $item['image_url'],
                    'name' => $item['data_name'],
                    'folder_id' => $item['processed_id']
                ];
            });

            return view('processed.edit')
                    ->with('userLogged',$this->userLogged)
                    ->with('processed',Processed::findOrFail($id))
                    ->with('trucks',Trucks::get())
                    ->with('adminSupervisorList',$adminSupervisorList)
                    ->with('images',$images)
                    ->with('arrayImageExist',$arrayImageExisting)
                    ->with('roles',$rolelist);

        }else {
            Session::flash('error','You dont have permission');
            return redirect()->route('processeds');
        }

    }

    public function update(editRequest $request, $id)
    {
        $this->userLogged = auth()->user();

        if($this->userLogged->hasRole('admin') || $this->userLogged->hasRole('supervisor') || $this->userLogged->hasRole('clerk')){

            //add img
            if($request->fileUploadArray) {

                $stringArray = $request->fileUploadArray;

                $fileArrayAdd =json_decode($stringArray);
                Processed_Images::where('processed_id',$id)->delete();

                foreach($fileArrayAdd as $file)
                {
                    if(File::exists(public_path('upload/'.$file->path))){
                        if(!File::exists(public_path('upload/'.$id))){
                            File::makeDirectory('upload/'.$id);
                        }

                        File::move(public_path('upload/'.$file->path), public_path('upload/'.$id.'/'.$file->path));
                    }

                    $processed_img = Processed_Images::create([
                        'caption' => $file->caption,
                        'image_url' => $file->path,
                        'image_ori_name' => $file->ori_path,
                        'mime_type' => $file->mime,
                        'processed_id' => $id,
                        'data_name' => $file->name
                    ]);
                }
            }
            //remove img
            if($request->fileUploadRemove) {

                $stringArrayRemove = $request->fileUploadRemove;
                $fileArray =json_decode($stringArrayRemove);

                foreach($fileArray as $file)
                {
                    // remove from public
                    $path = public_path().'/upload/'.$id.'/'.$file;
                    if (file_exists($path)) unlink($path);
                    // remove from db
                    $removeImageProcessedImage = Processed_Images::where('processed_id',$id)
                        ->where('image_url',$file)
                        ->delete();
                }
            }

            $processed = Processed::findOrFail($id);

            if($request->ticket_no) $processed->ticket_no = $request->ticket_no;
            if($request->supplier) $processed->supplier = $request->supplier;
            if($request->transporter) $processed->transporter = $request->transporter;
            if($request->truck) $processed->truck_id = $request->truck;
            if($request->product) $processed->product = $request->product;
            if($request->weight) $processed->weight = $request->weight;
            if($request->transport || $request->transport == '0') $processed->transport = $request->transport;
            if($request->goods_price) $processed->goods_price = $request->goods_price;
            if($request->moisture) $processed->moisture = $request->moisture;
            if($request->wastage) $processed->wastage = $request->wastage;
            if($request->remark) $processed->remark = $request->remark;
            if($request->issued_by) $processed->issued_by = $request->issued_by;
            if($request->created_at) $processed->created_at = $request->created_at;

            if($this->userLogged->hasRole('admin')){
                if($request->processed_status){
                    if($request->processed_status == 'finish'){
                        $processed->processed_status = true;
                        $processed->processed_date =now();
                    }
                }else {
                    $processed->processed_status = false;
                    $processed->processed_date = null;
                }
            }

            $processed->save();

            Session::flash('success','Processed updated successfully');

        }else { Session::flash('error','You dont have permission'); }

        return redirect()->route('processeds');
    }

    public function updateToggle(Request $request)
    {
        $this->userLogged = auth()->user();

        if($this->userLogged->hasRole('supervisor')){

            if($request->id && $request->status){

                $processed = Processed::findOrFail($request->id);

                if($request->status == 'true'){
                    $processed->processed_status = true;
                    $processed->processed_date = now();
                }else{
                    return response()->json([
                        "success" => false,
                        "message" => "Cannot unmark status"
                    ]);
                }
                $processed->save();

                return response()->json([
                    "success" => true,
                    "message" => "Processed updated successfully"
                ]);

            }
        }else {
            return response()->json([
                "success" => false,
                "message" => "You dont have permission"
            ]);
        }

    }

    public function create()
    {
        $this->userLogged = auth()->user();

        //get admin/supervisor for 'issued_by'
        $users_roles = DB::table('users_roles')
                    ->where('role_id',1)
                    ->orWhere('role_id',2)
                    ->orWhere('role_id',3)
                    ->orWhere('role_id',4)
                    ->pluck('user_id');

        $adminSupervisorList = User::whereIn('id',$users_roles)->get();

        return view('processed.create')
                ->with('userLogged',$this->userLogged)
                ->with('trucks',Trucks::get())
                ->with('adminSupervisorList',$adminSupervisorList);
    }

    public function store(createRequest $request)
    {
        $this->userLogged = auth()->user();

        $issuedBy = $this->userLogged->id;

        if($request->issued_by) $issuedBy = $request->issued_by;

        $final_weight = null;

        if(is_numeric($request->total_weight) && is_numeric($request->weight)) {
            $final_weight = bcsub($request->total_weight,$request->weight, 3);
        }

        $processed_status = false;
        $processed_date = null;

        if($request->processed_status){
            if($request->processed_status == 'finish'){
                $processed_status = true;
                $processed_date = now();
            }
        }else {
            $processed_status = false;
            $processed_date = null;
        }

        $created_at_date = null;
        if($request->created_at){
            $created_at_date = $request->created_at;
        }else $created_at_date = now();

        // form detail
        $processed = Processed::create([
            'ticket_no' => $request->ticket_no,
            'supplier' => $request->supplier,
            'transporter'=> $request->transporter,
            'truck_id'=> $request->truck,
            'product'=> $request->product,
            'weight'=> $request->weight,
            'transport'=> $request->transport,
            'goods_price'=>$request->goods_price,
            'wastage'=> $request->wastage,
            'remark'=> $request->remark,
            'moisture'=> $request->moisture,
            'issued_by'=> $issuedBy,
            'processed_status' => $processed_status,
            'processed_date' => $processed_date,
            'created_at' => $created_at_date
        ]);

        if($processed){

            // images

            if($request->fileUploadArray) {

                $stringArray = $request->fileUploadArray;

                $fileArray =json_decode($stringArray);

                foreach($fileArray as $file)
                {
                    // if(File::exists(public_path('upload',$file->path))){
                    //     File::move(public_path('upload',$file->path), public_path('upload/'.$request->ticket_no,$file->path));
                    // }
                    if(File::exists(public_path('upload',$file->path))){
                        if(!File::exists(public_path('upload/'.$processed->id))){
                            File::makeDirectory('upload/'.$processed->id);
                        }

                        File::move(public_path('upload/'.$file->path), public_path('upload/'.$processed->id.'/'.$file->path));
                    }

                    $processed_img = Processed_Images::create([
                        'caption' => $file->caption,
                        'image_url' => $file->path,
                        'image_ori_name' => $file->ori_path,
                        'mime_type' => $file->mime,
                        'processed_id' => $processed->id,
                        'data_name' => $file->data_name
                    ]);
                }

            }

            Session::flash('success','Processed Created successfully');
        }

       return redirect()->route('processeds');
    }

    public function delete($id)
    {
        $this->userLogged = auth()->user();
        if($this->userLogged->hasRole('admin')){
            try{
                $processed = Processed::findOrFail($id);
                $processed->delete();

                $processed_img = Processed_Images::where('processed_id',$id);
                $processed_img->delete();

                Session::flash('success','Processed deleted successfully');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }
        }else {
            Session::flash('error','You dont have permission');
            return redirect()->route('processeds');
        };


    }

    // --------------------- [ Upload image ] -----------------------------
    public function uploadImages(Request $request) {

        $request->validate([
            'file' => 'max:10240|mimes:jpeg,png,jpg,pdf'
        ],
        [
            'file.max' => 'File must not be greater than 10mb!',
        ]);

        $image = $request->file('file');
        // $image->extension()
        $imageOriginalName = $image->getClientOriginalName();

        $randomNumber = random_int(100000000, 999999999);
        $randomName = $randomNumber.'.'.$image->getClientOriginalExtension();

        $image->move(public_path('upload'),$randomName);

        $info = pathinfo($imageOriginalName);
        //$only_file_name =  $info['filename'];
        $mimeType = $image->getClientMimeType();

        //$data  =   ImageUpload::create(['image_name' => $imageName]);
        return response()->json([
            "status" => "success",
            "ori_path" => $imageOriginalName,
            "img_path" => $randomName,
            "name" => $randomNumber,
            "mime" => $mimeType
        ]);
    }

    // --------------------- [ Delete image ] -----------------------------

    public function deleteImage(Request $request) {
        $image = $request->file('filename');
        $filename =  $request->get('filename');
        //ImageUpload::where('image_name', $filename)->delete();
        $path = public_path().'/upload/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }
}
