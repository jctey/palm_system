<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Models\User;
use App\Models\Processed;
use App\Models\Processed_Images;
use App\Models\Shipment;
use App\Models\Shipment_Images;
use App\Models\Trucks;
use Illuminate\Support\Facades\Validator;
use DateTime;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;
use Jenssegers\Agent\Agent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
        //dd(auth()->user());
        // dd($user->hasRole('developer')); //will return true, if user has role
        // dd($user->givePermissionsTo('create-tasks'));// will return permission, if not null
        // dd($user->can('create-tasks'));

        //dd($user->hasRole('admin'));
        //dd($user->can('create-users'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();

        if($user->hasRole('admin') || $user->hasRole('supervisor') || $user->hasRole('clerk') || $user->hasRole('manager')){
            return view('home')
            ->with('user',$user);
        }else{
            return view('dashboard')
            ->with('user',$user);
        }

    }

    public function dashboard()
    {
        $user = auth()->user();
        $agent = new Agent;

        // $agent->isMobile(), $agent->isDesktop(), $agent->isTablet(), $agent->isPhone()
        if ($agent->isMobile() || $agent->isPhone()) {
            return view('mobile')
            ->with('user',$user);
        }else {
            return view('dashboard')
            ->with('user',$user);
        }


    }

    public function filter(Request $request)
    {
        $user = auth()->user();
        $arrayDate = $request->date;
        $periodArrayList = [];
        $dateRangeForVIew;
        $totalAmount = 0;
        $totalWeightInStock = 0;
        $totalProcessed = 0;
        $totalUnprocessed = 0;

        $periodArrayListShipment = [];
        $totalShipment = 0;
        $totalPurchase = 0;
        $totalShipmentWeight = 0;
        $totalSale = 0;
        // if($request->refresh) return View::make("partials/dashboard/collapse");

        if(is_array($arrayDate)){
            if(count($arrayDate) == 1){
                //single select date
                $validDate = $this->validateDateTime($arrayDate[0], 'Y-m-d');
                $dateRangeForVIew = $arrayDate[0];
                $dateRangeForVIew = str_replace("-","/",$dateRangeForVIew);

                if($validDate == true){

                    $totalEachProcessed = 0;
                    $totalEachUnprocessed = 0;
                    $totalEachShipment = 0;

                    $processedsAll = Processed::all();
                    $shipmentsAll = Shipment::all();

                    $date = new Carbon($arrayDate[0]);
                    $d = $date->format('Y-m-d');
                    // check date same
                    $processeds = $processedsAll->filter(function ($processed) use ($d) {
                        $processed->processed_date = new Carbon($processed->processed_date);

                        if($processed->processed_date->isSameDay($d) ||
                            ($processed->created_at->isSameDay($d) &&
                            ($processed->processed_date->isSameDay($d) == false))  ){
                            return $processed;
                        }
                    });

                    $shipments = $shipmentsAll->filter(function ($shipment) use ($d) {
                        $shipment->created_at = new Carbon($shipment->created_at);

                        if($shipment->created_at->isSameDay($d)){
                            return $shipment;
                        }
                    });

                    if($processeds->count() <= 0){
                        $processeds = null;
                    }else{
                        foreach($processeds as $p){
                            if(!empty($p->truck_id)){
                                $p->truck_id = Trucks::where('id',$p->truck_id)->first()->truck_plate_number;
                            }else $p->truck_id = 'Empty';

                            $p->issued_by = User::where('id',$p->issued_by)->first()->name;
                            $p->images = Processed_Images::where('processed_id',$p->id)->get();

                            //select today with borrowing unprocessed data
                            if($date->isToday() && $p->processed_status == 0){
                                if(!empty($p->weight)){
                                    $totalWeightInStock = $totalWeightInStock + $p->weight;
                                }
                                if(!empty($p->weight) && !empty($p->goods_price)){
                                    $totalAmount = $totalAmount + ($p->goods_price * $p->weight);
                                }
                            }
                            //select not today
                            if(!$p->processed_date->isSameDay($date)){
                                if(!empty($p->weight)){
                                    $totalWeightInStock = $totalWeightInStock + $p->weight;
                                }
                                if(!empty($p->weight) && !empty($p->goods_price)){
                                    $totalAmount = $totalAmount + ($p->goods_price * $p->weight);
                                }
                            }

                            if(($p->processed_status == true)&&($p->processed_date->isSameDay($d))) {
                                $totalProcessed = $totalProcessed + 1;
                                $totalEachProcessed = $totalEachProcessed + 1;
                            }
                            else {
                                $totalUnprocessed = $totalUnprocessed + 1;
                                $totalEachUnprocessed = $totalEachUnprocessed + 1;
                            }
                        }

                    }
                    array_push($periodArrayList,(object) [
                        'date' => $date->format('Y-m-d'),
                        'data' => $processeds,
                        'each_total_processed'=>$totalEachProcessed,
                        'each_total_unprocessed'=>$totalEachUnprocessed
                    ]);

                    if($shipments->count() <= 0){
                        $shipments = null;
                    }else{
                        foreach($shipments as $s){

                            $s->images = Shipment_Images::where('shipment_id',$s->id)->get();

                            if(!empty($s->purchase_value)) $totalPurchase = $totalPurchase + $s->purchase_value;
                            if(!empty($s->total_weight)) $totalShipmentWeight = $totalShipmentWeight + $s->total_weight;
                            if(!empty($s->sale)) $totalSale = $totalSale + $s->sale;

                            if($s->created_at->isSameDay($d)) {
                                $totalShipment = $totalShipment + 1;
                                $totalEachShipment = $totalEachShipment +1;
                            }

                        }
                    }

                    array_push($periodArrayListShipment,(object) [
                        'date' => $date->format('Y-m-d'),
                        'data' => $shipments,
                        'each_total_shipment' => $totalEachShipment
                    ]);

                }

            }else if(count($arrayDate) == 2){
                //multiple select date
                $validDate1 = $this->validateDateTime($arrayDate[0], 'Y-m-d');
                $validDate2 = $this->validateDateTime($arrayDate[1], 'Y-m-d');

                $dateRangeForVIew = $arrayDate[0]." ~ ".$arrayDate[1];
                $dateRangeForVIew = str_replace("-","/",$dateRangeForVIew);

                if(($validDate1 == true) && ($validDate2 == true)){

                    $period = CarbonPeriod::create($arrayDate[0], $arrayDate[1]);

                    // each date within range
                    $shipment_minus = Shipment::sum('total_weight');
                    foreach ($period as $date) {

                        $totalEachProcessed = 0;
                        $totalEachUnprocessed = 0;
                        $totalEachShipment = 0;

                        $processedsAll = Processed::all();
                        $shipmentsAll = Shipment::all();

                        $d = $date->format('Y-m-d');
                        // check date same
                        $processeds = $processedsAll->filter(function ($processed) use ($d) {
                            $processed->processed_date = new Carbon($processed->processed_date);

                            if($processed->processed_date->isSameDay($d) || ($processed->created_at->isSameDay($d) && ($processed->processed_date->isSameDay($d) == false))  ){
                                return $processed;
                            }

                        });

                        $shipments = $shipmentsAll->filter(function ($shipment) use ($d) {
                            $shipment->created_at = new Carbon($shipment->created_at);

                            if($shipment->created_at->isSameDay($d)){
                                return $shipment;
                            }
                        });

                        if($processeds->count() <= 0){
                            $processeds = null;
                        }else{

                            foreach($processeds as $p){

                                if(!empty($p->truck_id)){
                                    $p->truck_id = Trucks::where('id',$p->truck_id)->first()->truck_plate_number;
                                }else $p->truck_id = 'Empty';

                                $p->issued_by = User::where('id',$p->issued_by)->first()->name;
                                $p->images = Processed_Images::where('processed_id',$p->id)->get();


                                //select today with borrowing unprocessed data
                                if($date->isToday() && $p->processed_status == 0){
                                    if(!empty($p->weight)){
                                        $totalWeightInStock = $totalWeightInStock + $p->weight;
                                    }
                                    if(!empty($p->weight) && !empty($p->goods_price)){
                                        $totalAmount = $totalAmount + ($p->goods_price * $p->weight);
                                    }
                                }
                                //select not today
                                if(!$p->processed_date->isSameDay($date)){
                                    if(!empty($p->weight)){
                                        $totalWeightInStock = $totalWeightInStock + $p->weight;
                                    }
                                    if(!empty($p->weight) && !empty($p->goods_price)){
                                        $totalAmount = $totalAmount + ($p->goods_price * $p->weight);
                                    }
                                }

                                if(($p->processed_status == true)&&($p->processed_date->isSameDay($d))) {
                                    $totalProcessed = $totalProcessed + 1;
                                    $totalEachProcessed = $totalEachProcessed + 1;
                                }
                                else {
                                    $totalUnprocessed = $totalUnprocessed + 1;
                                    $totalEachUnprocessed = $totalEachUnprocessed + 1;
                                }

                            }



                        }

                        array_push($periodArrayList,(object) [
                            'date' => $date->format('Y-m-d'),
                            'data' => $processeds,
                            'each_total_processed'=>$totalEachProcessed,
                            'each_total_unprocessed'=>$totalEachUnprocessed
                        ]);

                        if($shipments->count() <= 0){
                            $shipments = null;
                        }else{
                            foreach($shipments as $s){

                                $s->images = Shipment_Images::where('shipment_id',$s->id)->get();

                                if(!empty($s->purchase_value)) $totalPurchase = $totalPurchase + $s->purchase_value;
                                if(!empty($s->total_weight)) $totalShipmentWeight = $totalShipmentWeight + $s->total_weight;
                                if(!empty($s->sale)) $totalSale = $totalSale + $s->sale;

                                if($s->created_at->isSameDay($d)) {
                                    $totalShipment = $totalShipment + 1;
                                    $totalEachShipment = $totalEachShipment + 1;
                                }

                            }
                        }

                        array_push($periodArrayListShipment,(object) [
                            'date' => $date->format('Y-m-d'),
                            'data' => $shipments,
                            'each_total_shipment' => $totalEachShipment
                        ]);
                    }

                }

            }

        }
        // get processed unprocessed
        if(count($periodArrayList) > 0){

        }

        return View::make("partials/dashboard/collapse", [
            "dataFiltered" => $periodArrayList,
            'total_processed' => $totalProcessed,
            'total_unprocessed' => $totalUnprocessed,
            'date_range' => $dateRangeForVIew,
            'total_amount' => number_format((float)$totalAmount - $totalPurchase, 2,'.', ''),
            'total_weight_stock' => number_format((float)$totalWeightInStock - $totalShipmentWeight,3,'.', ''),

            'total_ready_stock' => number_format((float)$this->calculateTotalReadyStock(),3,'.', ''),
            "dataFilteredShipment" => $periodArrayListShipment,
            'total_shipment' => $totalShipment,
            'total_purchase' => number_format((float)$totalPurchase,2,'.', ''),
            'total_sale' => number_format((float)$totalSale,2,'.', ''),

            'userLogged'=>$user

        ]);
    }
    public function filterMobile(Request $request)
    {
        $user = auth()->user();
        $arrayDate = $request->date;
        $periodArrayList = [];
        $dateRangeForVIew;
        $totalAmount = 0;
        $totalWeightInStock = 0;
        $totalProcessed = 0;
        $totalUnprocessed = 0;

        $periodArrayListShipment = [];
        $totalShipment = 0;
        $totalPurchase = 0;
        $totalShipmentWeight = 0;
        $totalSale = 0;
        // if($request->refresh) return View::make("partials/dashboard/collapse");

        if(is_array($arrayDate)){
            if(count($arrayDate) == 1){

                $validDate = $this->validateDateTime($arrayDate[0], 'Y-m-d');
                $dateRangeForVIew = $arrayDate[0];
                $dateRangeForVIew = str_replace("-","/",$dateRangeForVIew);

                if($validDate == true){

                    $totalEachProcessed = 0;
                    $totalEachUnprocessed = 0;
                    $totalEachShipment = 0;

                    $shipment_minus = Shipment::sum('total_weight');
                    $processedsAll = Processed::all();
                    $shipmentsAll = Shipment::all();

                    $date = new Carbon($arrayDate[0]);
                    $d = $date->format('Y-m-d');
                    // check date same
                    $processeds = $processedsAll->filter(function ($processed) use ($d) {
                        $processed->processed_date = new Carbon($processed->processed_date);

                        if($processed->processed_date->isSameDay($d) ||
                            ($processed->created_at->isSameDay($d) &&
                            ($processed->processed_date->isSameDay($d) == false))  ){
                            return $processed;
                        }
                    });

                    $shipments = $shipmentsAll->filter(function ($shipment) use ($d) {
                        $shipment->created_at = new Carbon($shipment->created_at);

                        if($shipment->created_at->isSameDay($d)){
                            return $shipment;
                        }
                    });

                    if($processeds->count() <= 0){
                        $processeds = null;
                    }else{
                        foreach($processeds as $p){
                            if(!empty($p->truck_id)){
                                $p->truck_id = Trucks::where('id',$p->truck_id)->first()->truck_plate_number;
                            }else $p->truck_id = 'Empty';

                            $p->issued_by = User::where('id',$p->issued_by)->first()->name;
                            $p->images = Processed_Images::where('processed_id',$p->id)->get();

                            //select today with borrowing unprocessed data
                            if($date->isToday() && $p->processed_status == 0){
                                if(!empty($p->weight)){
                                    $totalWeightInStock = $totalWeightInStock + $p->weight;
                                }
                                if(!empty($p->weight) && !empty($p->goods_price)){
                                    $totalAmount = $totalAmount + ($p->goods_price * $p->weight);
                                }
                            }
                            //select not today
                            if(!$p->processed_date->isSameDay($date)){
                                if(!empty($p->weight)){
                                    $totalWeightInStock = $totalWeightInStock + $p->weight;
                                }
                                if(!empty($p->weight) && !empty($p->goods_price)){
                                    $totalAmount = $totalAmount + ($p->goods_price * $p->weight);
                                }
                            }


                            if(($p->processed_status == true)&&($p->processed_date->isSameDay($d))) {
                                $totalProcessed = $totalProcessed + 1;
                                $totalEachProcessed = $totalEachProcessed + 1;
                            }
                            else {
                                $totalUnprocessed = $totalUnprocessed + 1;
                                $totalEachUnprocessed = $totalEachUnprocessed + 1;
                            }
                        }

                    }
                    array_push($periodArrayList,(object) [
                        'date' => $date->format('Y-m-d'),
                        'data' => $processeds,
                        'each_total_processed'=>$totalEachProcessed,
                        'each_total_unprocessed'=>$totalEachUnprocessed
                    ]);

                    if($shipments->count() <= 0){
                        $shipments = null;
                    }else{
                        foreach($shipments as $s){

                            $s->images = Shipment_Images::where('shipment_id',$s->id)->get();

                            if(!empty($s->purchase_value)) $totalPurchase = $totalPurchase + $s->purchase_value;
                            if(!empty($s->total_weight)) $totalShipmentWeight = $totalShipmentWeight + $s->total_weight;
                            if(!empty($s->sale)) $totalSale = $totalSale + $s->sale;

                            if($s->created_at->isSameDay($d)) {
                                $totalShipment = $totalShipment + 1;
                                $totalEachShipment = $totalEachShipment +1;
                            }

                        }
                    }

                    array_push($periodArrayListShipment,(object) [
                        'date' => $date->format('Y-m-d'),
                        'data' => $shipments,
                        'each_total_shipment' => $totalEachShipment
                    ]);

                }

            }else if(count($arrayDate) == 2){

                $validDate1 = $this->validateDateTime($arrayDate[0], 'Y-m-d');
                $validDate2 = $this->validateDateTime($arrayDate[1], 'Y-m-d');

                $dateRangeForVIew = $arrayDate[0]." ~ ".$arrayDate[1];
                $dateRangeForVIew = str_replace("-","/",$dateRangeForVIew);

                if(($validDate1 == true) && ($validDate2 == true)){

                    $period = CarbonPeriod::create($arrayDate[0], $arrayDate[1]);

                    // each date within range
                    $shipment_minus = Shipment::sum('total_weight');
                    foreach ($period as $date) {

                        $totalEachProcessed = 0;
                        $totalEachUnprocessed = 0;
                        $totalEachShipment = 0;

                        $processedsAll = Processed::all();
                        $shipmentsAll = Shipment::all();

                        $d = $date->format('Y-m-d');
                        // check date same
                        $processeds = $processedsAll->filter(function ($processed) use ($d) {
                            $processed->processed_date = new Carbon($processed->processed_date);

                            if($processed->processed_date->isSameDay($d) || ($processed->created_at->isSameDay($d) && ($processed->processed_date->isSameDay($d) == false))  ){
                                return $processed;
                            }

                        });

                        $shipments = $shipmentsAll->filter(function ($shipment) use ($d) {
                            $shipment->created_at = new Carbon($shipment->created_at);

                            if($shipment->created_at->isSameDay($d)){
                                return $shipment;
                            }
                        });

                        if($processeds->count() <= 0){
                            $processeds = null;
                        }else{

                            foreach($processeds as $p){

                                if(!empty($p->truck_id)){
                                    $p->truck_id = Trucks::where('id',$p->truck_id)->first()->truck_plate_number;
                                }else $p->truck_id = 'Empty';

                                $p->issued_by = User::where('id',$p->issued_by)->first()->name;
                                $p->images = Processed_Images::where('processed_id',$p->id)->get();


                                //select today with borrowing unprocessed data
                                if($date->isToday() && $p->processed_status == 0){
                                    if(!empty($p->weight)){
                                        $totalWeightInStock = $totalWeightInStock + $p->weight;
                                    }
                                    if(!empty($p->weight) && !empty($p->goods_price)){
                                        $totalAmount = $totalAmount + ($p->goods_price * $p->weight);
                                    }
                                }
                                //select not today
                                if(!$p->processed_date->isSameDay($date)){
                                    if(!empty($p->weight)){
                                        $totalWeightInStock = $totalWeightInStock + $p->weight;
                                    }
                                    if(!empty($p->weight) && !empty($p->goods_price)){
                                        $totalAmount = $totalAmount + ($p->goods_price * $p->weight);
                                    }
                                }


                                if(($p->processed_status == true)&&($p->processed_date->isSameDay($d))) {
                                    $totalProcessed = $totalProcessed + 1;
                                    $totalEachProcessed = $totalEachProcessed + 1;
                                }
                                else {
                                    $totalUnprocessed = $totalUnprocessed + 1;
                                    $totalEachUnprocessed = $totalEachUnprocessed + 1;
                            }

                            }

                        }

                        array_push($periodArrayList,(object) [
                            'date' => $date->format('Y-m-d'),
                            'data' => $processeds,
                            'each_total_processed'=>$totalEachProcessed,
                            'each_total_unprocessed'=>$totalEachUnprocessed
                        ]);

                        if($shipments->count() <= 0){
                            $shipments = null;
                        }else{
                            foreach($shipments as $s){

                                $s->images = Shipment_Images::where('shipment_id',$s->id)->get();

                                if(!empty($s->purchase_value)) $totalPurchase = $totalPurchase + $s->purchase_value;
                                if(!empty($s->total_weight)) $totalShipmentWeight = $totalShipmentWeight + $s->total_weight;
                                if(!empty($s->sale)) $totalSale = $totalSale + $s->sale;

                                if($s->created_at->isSameDay($d)) {
                                    $totalShipment = $totalShipment + 1;
                                    $totalEachShipment = $totalEachShipment + 1;
                                }

                            }
                        }

                        array_push($periodArrayListShipment,(object) [
                            'date' => $date->format('Y-m-d'),
                            'data' => $shipments,
                            'each_total_shipment' => $totalEachShipment
                        ]);
                    }

                }

            }

        }
        // get processed unprocessed
        if(count($periodArrayList) > 0){

        }

        return View::make("partials/mobile_dashboard", [
            "dataFiltered" => $periodArrayList,
            'total_processed' => $totalProcessed,
            'total_unprocessed' => $totalUnprocessed,
            'date_range' => $dateRangeForVIew,
            'total_amount' => number_format((float)$totalAmount - $totalPurchase, 2,'.', ''),
            'total_weight_stock' => number_format((float)$totalWeightInStock - $totalShipmentWeight,3,'.', ''),

            'total_ready_stock' => number_format((float)$this->calculateTotalReadyStock(),3,'.', ''),
            "dataFilteredShipment" => $periodArrayListShipment,
            'total_shipment' => $totalShipment,
            'total_purchase' => number_format((float)$totalPurchase,2,'.', ''),
            'total_sale' => number_format((float)$totalSale,2,'.', ''),

            'userLogged'=>$user

        ]);
    }


    function validateDateTime($dateStr, $format)
    {
        date_default_timezone_set('UTC');
        $date = DateTime::createFromFormat($format, $dateStr);
        return $date && ($date->format($format) === $dateStr);
    }

    function calculateTotalReadyStock(){
        $shipment_minus = 0;
        $trs = 0;

        $shipment_minus = Shipment::sum('total_weight');
        $trs = Processed::where('processed_status',true)->sum('weight');

        return ($trs - $shipment_minus);
    }


}
