<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trucks;
use App\Models\Processed;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\truck\createRequest;
use Session;
use View;
use DataTables;

class TruckController extends Controller
{
    private $userLogged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->userLogged = auth()->user();

        return view('trucks.index')
                ->with('userLogged',$this->userLogged);
    }

    public function listDatatable(Request $req)
    {
        $trucks = Trucks::select(['id','truck_plate_number','created_at'])->orderBy('created_at','ASC');

        return Datatables::of($trucks)
            ->addIndexColumn()
            ->addColumn('action', function ($trucks) {
                return '<div style="display:flex; justify-content: space-evenly;"><button data-toggle="modal" data-target="#confirmModal" data-modaltype="truck"  data-truckid='.$trucks->id.' class="delete btn btn-danger btn-sm shadow"><i class="fa fa-trash" aria-hidden="true"></i></button></div';
            })
            ->addColumn('count', function ($trucks) {
                $processed_of_truck = Processed::where('truck_id',$trucks->id)->get();
                return $processed_of_truck->count();
            })
            ->editColumn('id', '{{$id}}')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function delete($id)
    {
        try{
            $countOfProcessed = Processed::where('truck_id',$id)->get();

            if($countOfProcessed->count() > 0){
                Session::flash('error','Truck are used cannot be delete');
            }else{
                $truck = Trucks::findOrFail($id);
                $truck->delete();

                Session::flash('success','Truck deleted successfully');
            }

            return redirect()->back();

        }catch(Exception $e) {
            return Session::flash('error','Something wrong');
        }

    }

    //  store from processed
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'truck_plate_number' => 'required|max:10|min:4|unique:trucks',
        ]);

        if ($validator->fails()) {

            return response()->json(['errors'=>$validator->errors()->all()]);

        }

        $truck = Trucks::create([
            'truck_plate_number' => $request->truck_plate_number,
        ]);

        if($truck) Session::flash('success','Vehicle Added successfully');

        return response()->json(['success'=>true, 'truck'=>$truck]);

    }

    public function generateList(){

        return View::make("partials/processed/truckLoop", ["trucks" => Trucks::get()]);
    }
}
