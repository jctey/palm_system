<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\users\createRequest;
use App\Http\Requests\users\editRequest;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    private $userLogged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->userLogged = auth()->user();

        return view('users.index')
                ->with('userLogged',$this->userLogged);
    }

    public function userListDatatable(Request $req)
    {
        $users = User::select(['id','name', 'email','created_at'])->orderBy('created_at','ASC');

        $users_roles = DB::table('users_roles')->get();

        if($req->verify){
            if($req->verify == 'verify') $users = $users->whereNotNull('email_verified_at');
            else $users = $users->WhereNull('email_verified_at');
        }
        if($req->role){

            if($req->role == 'admin') {
                $idlist = $users_roles->where('role_id',1)->pluck('user_id');
                $users = $users->whereIn('id',$idlist);
            }
            if($req->role == 'supervisor') {
                $idlist = $users_roles->where('role_id',2)->pluck('user_id');
                $users = $users->whereIn('id',$idlist);
            }
            if($req->role == 'shareholder') {
                $idlist = $users_roles->where('role_id',3)->pluck('user_id');
                $users = $users->whereIn('id',$idlist);
            }
        }
        if($req->user_verified){
            if($req->user_verified == 'user_verified') $users = $users->where('user_verified','=',true);
            if($req->user_verified == 'user_unverified') $users = $users->where('user_verified','=',false);
        }

        return Datatables::of($users)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                if($user->id != 1){
                    return '<div style="display:flex; justify-content: space-evenly;"><a href="'.route('user-edit',$user->id).'" class="edit btn btn-info btn-sm shadow"> Edit</a><button data-toggle="modal" data-target="#confirmModal" data-modaltype="user" data-useremail='.$user->email.' data-userid='.$user->id.' class="delete btn btn-danger btn-sm shadow">Delete</button></div';
                }
            })

            ->editColumn('id', '{{$id}}')

            ->editColumn('created_at', function ($user) {
                $formatedDate = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('d/m/Y G:i:s A');
                return $formatedDate;
            })

            ->rawColumns(['action'])

            ->make(true);
    }

    public function edit( $id)
    {
        $this->userLogged = auth()->user();

        $role_id = DB::table('users_roles')->select('*')->where('user_id',$id)->first();

        $rolelist = Role::get();

        return view('users.edit')
                ->with('userLogged',$this->userLogged)
                ->with('user',User::findOrFail($id))
                ->with('role_id', $role_id->role_id)
                ->with('roles',$rolelist);
    }

    public function update(editRequest $request, $id)
    {
        $user = User::findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;

        if($request->role){
            $selectedRoleId = $request->role[0];
            // 1 admin, 2 supervisor, 3 shareholder

            $role = Role::where('id', $selectedRoleId)->first();
            $user->roles()->sync($role);
        }

        if($request->verify == 'verify') $user->markEmailAsVerified();

        if($request->user_verified == 'user_verified') {
            $user->user_verified = true;
        }else $user->user_verified = false;

        if($request->password) $user->password = Hash::make($request->password);

        $user->save();

        Session::flash('success','User updated successfully');

        return redirect()->route('users');
    }

    public function create()
    {
        $this->userLogged = auth()->user();

        $rolelist = Role::get();

        return view('users.create')
                ->with('userLogged',$this->userLogged)
                ->with('roles',$rolelist);
    }

    public function store(createRequest $request)
    {
        $userVerified = false;
        if($request->user_verified == 'user_verified') $userVerified = true;

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email ,
            'password'=> Hash::make($request->password),
            'user_verified'=> $userVerified
        ]);

        if($user){
            //verify email
            if($request->verify == 'verify') $user->markEmailAsVerified();

            if($request->role){
                $selectedRoleId = $request->role[0];
                // 1 admin, 2 supervisor, 3 shareholder
                if($selectedRoleId == 1 || $selectedRoleId == 2) $user->markEmailAsVerified();
                else $user->sendEmailVerificationNotification();

                $role = Role::where('id', $selectedRoleId)->first();
                $user->roles()->attach($role);
            }

            Session::flash('success','User Created successfully');
        }

        return redirect()->route('users');
    }

    public function delete($id)
    {
        if($id == 1){
            return Session::flash('error','Cannot delete admin');
        }else{
            try{
                $user = User::findOrFail($id);
                $user->delete();

                Session::flash('success','User deleted successfully');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }
        }

    }
}
