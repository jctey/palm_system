<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Models\User;
use App\Models\Role;
use App\Models\Shipment;
use App\Models\Shipment_Images;
use App\Models\Processed;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;
use File;
use App\Http\Requests\shipment\createRequest;
use App\Http\Requests\shipment\editRequest;

class ShipmentController extends Controller
{
    private $userLogged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->userLogged = auth()->user();

        return view('shipments.index')
                ->with('userLogged',$this->userLogged);
    }

    public function listDatatable(Request $req)
    {
        $this->userLogged = auth()->user();

        $shipments = Shipment::select(['id','ship_name','draft_weight', 'draft_weight_value','total_weight','total_weight_value','purchase_value','sale','stock_usage','created_at'])->orderBy('created_at','ASC');

        return Datatables::of($shipments)
            ->addIndexColumn()
            ->addColumn('action', function ($shipment) {
                return '<div style="display:flex; justify-content: space-evenly;">
                    <a href="'.route('shipment-edit',$shipment->id).'" class="edit btn btn-info btn-sm shadow"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <button data-toggle="modal" data-target="#confirmModal" data-modaltype="shipment"  data-shipmentid='.$shipment->id.' class="delete btn btn-danger btn-sm shadow"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </div';
            })

            ->editColumn('id', '{{$id}}')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $this->userLogged = auth()->user();
        $totalReadyStock=0;
        //total ready stock
        $done_processed = Processed::where('processed_status',1)->get();
        $shipment = Shipment::sum('total_weight');

        if($done_processed->count() > 0){

            foreach ($done_processed as $p){
                if(!empty($p->weight)) {
                    $totalReadyStock = $totalReadyStock + $p->weight;
                }
            }

            $totalReadyStock = $totalReadyStock-$shipment;
        }

        return view('shipments.create')
                ->with('userLogged',$this->userLogged)
                ->with('totalReadyStock',number_format((float)$totalReadyStock,2,'.', ''));

    }

    public function store(createRequest $request)
    {
        $this->userLogged = auth()->user();

        $draft_weight=0;
        $draft_weight_value=0;
        $total_weight=0;
        $total_weight_value=0;
        $purchase_value=null;
        $sale=null;
        $moisture=null;
        $remark=null;

        if($request->draft_weight) $draft_weight = $request->draft_weight;
        if($request->draft_weight_value) $draft_weight_value = $request->draft_weight_value;
        if($request->total_weight) $total_weight = $request->total_weight;
        if($request->total_weight_value) $total_weight_value = $request->total_weight_value;
        if($request->purchase_value) $purchase_value = $request->purchase_value;
        if($request->sale) $sale = $request->sale;
        if($request->moisture) $moisture = $request->moisture;
        if($request->remark) $remark = $request->remark;

        $created_at_date = null;
        if($request->created_at){
            $created_at_date = $request->created_at;
        }else $created_at_date = now();

        // form detail
        $shipment = Shipment::create([
            'ship_name' => $request->ship_name,
            'draft_weight' => $draft_weight,
            'draft_weight_value'=> $draft_weight_value,
            'total_weight'=> $total_weight,
            'total_weight_value'=> $total_weight_value,
            'purchase_value'=> $purchase_value,
            'sale'=> $sale,
            'moisture'=> $moisture,
            'remark'=> $remark,
            'created_at' => $created_at_date
        ]);

        if($shipment){

            // images

            if($request->fileUploadArray) {

                $stringArray = $request->fileUploadArray;

                $fileArray =json_decode($stringArray);

                foreach($fileArray as $file)
                {

                    if(File::exists(public_path('upload',$file->path))){
                        if(!File::exists(public_path('upload/'.$shipment->id))){
                            File::makeDirectory('upload_shipment/'.$shipment->id);
                        }

                        File::move(public_path('upload/'.$file->path), public_path('upload_shipment/'.$shipment->id.'/'.$file->path));
                    }

                    $shipment_img = Shipment_Images::create([
                        'caption' => $file->caption,
                        'image_url' => $file->path,
                        'image_ori_name' => $file->ori_path,
                        'mime_type' => $file->mime,
                        'shipment_id' => $shipment->id,
                        'data_name' => $file->data_name
                    ]);
                }

            }

            Session::flash('success','Shipment Created successfully');
        }

       return redirect()->route('shipments');
    }

    public function edit($id)
    {
        $this->userLogged = auth()->user();

        if($this->userLogged->hasRole('admin') || $this->userLogged->hasRole('manager')){

            $images = Shipment_Images::where('shipment_id',$id)->get();

            $arrayImageExisting = [];
            $arrayImageExisting = $images->map(function($item, $key) {
                return [
                    'caption' => $item['caption'],
                    'mime' => $item['mime_type'],
                    'ori_path' => $item['image_ori_name'],
                    'path' => $item['image_url'],
                    'name' => $item['data_name'],
                    'folder_id' => $item['shipment_id']
                ];
            });

            $totalReadyStock=0;
            //total ready stock
            $done_processed = Processed::where('processed_status',1)->get();
            $shipment = Shipment::sum('total_weight');

            if($done_processed->count() > 0){

                foreach ($done_processed as $p){
                    if(!empty($p->weight)) {
                        $totalReadyStock = $totalReadyStock + $p->weight;
                    }
                }

                $totalReadyStock = $totalReadyStock-$shipment;
            }


            return view('shipments.edit')
                    ->with('userLogged',$this->userLogged)
                    ->with('shipment',Shipment::findOrFail($id))
                    ->with('images',$images)
                    ->with('arrayImageExist',$arrayImageExisting)
                    ->with('totalReadyStock',number_format((float)$totalReadyStock,2,'.', ''));

        }else {
            Session::flash('error','You dont have permission');
            return redirect()->route('shipments');
        }

    }

    public function update(editRequest $request, $id)
    {
        $this->userLogged = auth()->user();

        if($this->userLogged->hasRole('admin') || $this->userLogged->hasRole('manager')){

            //add img
            if($request->fileUploadArray) {

                $stringArray = $request->fileUploadArray;

                $fileArrayAdd =json_decode($stringArray);
                Shipment_Images::where('shipment_id',$id)->delete();

                foreach($fileArrayAdd as $file)
                {
                    if(File::exists(public_path('upload/'.$file->path))){
                        if(!File::exists(public_path('upload_shipment/'.$id))){
                            File::makeDirectory('upload_shipment/'.$id);
                        }
                        File::move(public_path('upload/'.$file->path), public_path('upload_shipment/'.$id.'/'.$file->path));
                    }

                    $shipment_img = Shipment_Images::create([
                        'caption' => $file->caption,
                        'image_url' => $file->path,
                        'image_ori_name' => $file->ori_path,
                        'mime_type' => $file->mime,
                        'shipment_id' => $id,
                        'data_name' => $file->name
                    ]);
                }
            }
            //remove img
            if($request->fileUploadRemove) {

                $stringArrayRemove = $request->fileUploadRemove;
                $fileArray =json_decode($stringArrayRemove);

                foreach($fileArray as $file)
                {
                    // remove from public
                    $path = public_path().'/upload_shipment/'.$id.'/'.$file;
                    if (file_exists($path)) unlink($path);
                    // remove from db
                    $removeImageShipmentImage = Shipment_Images::where('shipment_id',$id)
                        ->where('image_url',$file)
                        ->delete();
                }
            }

            $shipment = Shipment::findOrFail($id);

            if($request->ship_name) $shipment->ship_name = $request->ship_name;
            if($request->draft_weight) $shipment->draft_weight = $request->draft_weight;
            if($request->draft_weight_value) $shipment->draft_weight_value = $request->draft_weight_value;
            if($request->total_weight) $shipment->total_weight = $request->total_weight;
            if($request->total_weight_value) $shipment->total_weight_value = $request->total_weight_value;
            if($request->purchase_value) $shipment->purchase_value = $request->purchase_value;
            if($request->sale) $shipment->sale = $request->sale;
            if($request->moisture) $shipment->moisture = $request->moisture;
            if($request->remark) $shipment->remark = $request->remark;
            if($request->created_at) $shipment->created_at = $request->created_at;

            $shipment->save();

            Session::flash('success','Shipment updated successfully');

        }else { Session::flash('error','You dont have permission'); }

        return redirect()->route('shipments');
    }

    public function delete($id)
    {
        $this->userLogged = auth()->user();
        if($this->userLogged->hasRole('admin')){
            try{
                $shipment = Shipment::findOrFail($id);
                $shipment->delete();

                $shipment_img = Shipment_Images::where('shipment_id',$id);
                $shipment_img->delete();

                Session::flash('success','Shipment deleted successfully');

                return redirect()->back();

            }catch(Exception $e) {
                return Session::flash('error','Something wrong');
            }
        }else {
            Session::flash('error','You dont have permission');
            return redirect()->route('shipments');
        };


    }

    // --------------------- [ Upload image ] -----------------------------
    public function uploadImages(Request $request) {

        $request->validate([
            'file' => 'max:10240|mimes:jpeg,png,jpg,pdf'
        ],
        [
            'file.max' => 'File must not be greater than 10mb!',
        ]);

        $image = $request->file('file');
        // $image->extension()
        $imageOriginalName = $image->getClientOriginalName();

        $randomNumber = random_int(100000000, 999999999);
        $randomName = $randomNumber.'.'.$image->getClientOriginalExtension();

        $image->move(public_path('upload'),$randomName);

        $info = pathinfo($imageOriginalName);
        //$only_file_name =  $info['filename'];
        $mimeType = $image->getClientMimeType();

        //$data  =   ImageUpload::create(['image_name' => $imageName]);
        return response()->json([
            "status" => "success",
            "ori_path" => $imageOriginalName,
            "img_path" => $randomName,
            "name" => $randomNumber,
            "mime" => $mimeType
        ]);
    }

    // --------------------- [ Delete image ] -----------------------------

    public function deleteImage(Request $request) {
        $image = $request->file('filename');
        $filename =  $request->get('filename');
        //ImageUpload::where('image_name', $filename)->delete();
        $path = public_path().'/upload/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }

}
