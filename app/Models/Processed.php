<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Processed extends Model
{
    use HasFactory, LogsActivity;

    protected $fillable = [
        'id',
        'ticket_no',
        'supplier',
        'transporter',
        'truck_id',
        'product',
        'weight',
        'transport',
        'goods_price',
        'moisture',
        'wastage',
        'remark',
        'issued_by',
        'processed_status',
        'processed_date',
        'created_at',
        'total_weight_final',
        'price_kg'
    ];
    protected static $logAttributes = ['*'];

    protected $table = 'processeds';

    public function processedImages()
    {
        return $this->hasMany(Processed_Images::class);
    }

    public function truck()
    {
        return $this->belongsTo(Trucks::class,'truck_id');
    }

}
