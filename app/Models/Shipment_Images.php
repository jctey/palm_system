<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment_Images extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'caption',
        'image_url',
        'image_ori_name',
        'mime_type',
        'shipment_id',
        'data_name',
        'created_at',
    ];

    protected $table = 'shipment_images';

}
