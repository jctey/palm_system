<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Processed_Images extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'caption',
        'image_url',
        'image_ori_name',
        'mime_type',
        'processed_id',
        'data_name',
        'created_at',
    ];

    protected $table = 'processed_images';

    public function processed()
    {
        return $this->belongsTo(Processed::class,'processed_id');
    }
}
