<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'ship_name',
        'draft_weight',
        'draft_weight_value',
        'total_weight',
        'total_weight_value',
        'remark',
        'moisture',
        'purchase_value',
        'sale',
        'stock_usage',
        'created_at'
    ];

    protected $table = 'shipment';
}
