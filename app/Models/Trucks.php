<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trucks extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'truck_plate_number',
        'created_at',
    ];

    protected $table = 'trucks';

    public function processed()
    {
        return $this->hasMany(Processed::class);
    }

}
