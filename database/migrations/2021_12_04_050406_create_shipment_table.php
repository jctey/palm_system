<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment', function (Blueprint $table) {
            $table->id();
            $table->string('ship_name')->nullable();
            $table->string('draft_weight')->nullable();
            $table->string('draft_weight_value')->nullable();
            $table->string('total_weight')->nullable();
            $table->string('total_weight_value')->nullable();
            $table->string('remark')->nullable();
            $table->string('moisture')->nullable();
            $table->string('purchase_value')->nullable();
            $table->string('sale')->nullable();
            $table->string('stock_usage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment');
    }
}
