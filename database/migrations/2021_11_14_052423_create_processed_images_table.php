<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessedImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processed_images', function (Blueprint $table) {
            $table->id();
            $table->string('caption')->nullable();
            $table->string('image_url');
            $table->string('image_ori_name');
            $table->string('mime_type');
            $table->bigInteger('processed_id')->unsigned();
            $table->string('data_name')->nullable();
            $table->timestamps();

            $table->foreign('processed_id')->references('id')->on('processeds')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processed_images');
    }
}
