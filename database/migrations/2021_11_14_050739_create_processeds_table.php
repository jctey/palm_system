<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processeds', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('truck_id')->nullable();
            $table->string('ticket_no')->unique()->nullable();
            $table->string('product')->nullable();
            $table->string('weight')->nullable();
            $table->string('supplier')->nullable();
            $table->string('transporter')->nullable();
            $table->string('transport')->nullable();
            $table->string('goods_price')->nullable();
            $table->string('moisture')->nullable();
            $table->string('wastage')->nullable();
            $table->string('remark')->nullable();
            $table->bigInteger('issued_by')->unsigned();

            $table->boolean('processed_status')->default(0);
            $table->dateTime('processed_date')->nullable();
            $table->timestamps();

            $table->foreign('issued_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processeds');
    }
}
