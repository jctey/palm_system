<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_images', function (Blueprint $table) {
            $table->id();
            $table->string('caption')->nullable();
            $table->string('image_url');
            $table->string('image_ori_name');
            $table->string('mime_type');
            $table->bigInteger('shipment_id')->unsigned();
            $table->string('data_name')->nullable();
            $table->timestamps();

            $table->foreign('shipment_id')->references('id')->on('shipment')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_images');
    }
}
