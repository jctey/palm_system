<?php

namespace Database\Seeders;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//admin
        $permissions[0] = Permission::where('slug','create-users')->first();
        $permissions[1] = Permission::where('slug','edit-users')->first();
        $permissions[2] = Permission::where('slug','delete-users')->first();
        $permissions[3] = Permission::where('slug','create-processed')->first();
        $permissions[4] = Permission::where('slug','edit-processed')->first();
        $permissions[5] = Permission::where('slug','delete-processed')->first();
		$permissions[6] = Permission::where('slug','create-shipment')->first();
        $permissions[7] = Permission::where('slug','edit-shipment')->first();
        $permissions[8] = Permission::where('slug','delete-shipment')->first();

		$admin_role = new Role();
		$admin_role->slug = 'admin';
		$admin_role->name = 'Admin';
		$admin_role->save();

		for($i=0; $i<count($permissions) ;$i++){
			$admin_role->permissions()->attach($permissions[$i]);
		}

		//supervisor
		$supervisor_role = new Role();
		$supervisor_role->slug = 'supervisor';
		$supervisor_role->name = 'Supervisor';
		$supervisor_role->save();
		$supervisor_role->permissions()->attach($permissions[4]);

		//clerk
		$clerk_role = new Role();
		$clerk_role->slug = 'clerk';
		$clerk_role->name = 'Clerk';
		$clerk_role->save();

		$clerk_role->permissions()->attach($permissions[3]);
		$clerk_role->permissions()->attach($permissions[4]);

		//manager
		$manager_role = new Role();
		$manager_role->slug = 'manager';
		$manager_role->name = 'Manager';
		$manager_role->save();

		$manager_role->permissions()->attach($permissions[6]);
		$manager_role->permissions()->attach($permissions[7]);

		//shareholder
		$shareholder_role = new Role();
		$shareholder_role->slug = 'shareholder';
		$shareholder_role->name = 'Shareholder';
		$shareholder_role->save();

		$admin_role = Role::where('slug','admin')->first();
		$supervisor_role = Role::where('slug', 'supervisor')->first();
		$clerk_role = Role::where('slug', 'clerk')->first();
		$manager_role = Role::where('slug', 'manager')->first();

        //admin permission
		$createUsers = new Permission();
		$createUsers->slug = 'create-users';
		$createUsers->name = 'Create Users';
		$createUsers->save();
		$createUsers->roles()->attach($admin_role);

        $editUsers = new Permission();
		$editUsers->slug = 'edit-users';
		$editUsers->name = 'Edit Users';
		$editUsers->save();
		$editUsers->roles()->attach($admin_role);

        $deleteUsers = new Permission();
		$deleteUsers->slug = 'delete-users';
		$deleteUsers->name = 'Delete Users';
		$deleteUsers->save();
		$deleteUsers->roles()->attach($admin_role);

        $createProcessed = new Permission();
		$createProcessed->slug = 'create-processed';
		$createProcessed->name = 'Create Processed';
		$createProcessed->save();
		$createProcessed->roles()->attach($admin_role);
		$createProcessed->roles()->attach($clerk_role);

        $editProcessed = new Permission();
		$editProcessed->slug = 'edit-processed';
		$editProcessed->name = 'Edit Processed';
		$editProcessed->save();
		$editProcessed->roles()->attach($admin_role);
		$editProcessed->roles()->attach($supervisor_role);
		$editProcessed->roles()->attach($clerk_role);

        $deleteProcessed = new Permission();
		$deleteProcessed->slug = 'delete-processed';
		$deleteProcessed->name = 'Delete Processed';
		$deleteProcessed->save();
		$deleteProcessed->roles()->attach($admin_role);

		$createShipment = new Permission();
		$createShipment->slug = 'create-shipment';
		$createShipment->name = 'Create Shipment';
		$createShipment->save();
		$createShipment->roles()->attach($admin_role);
		$createShipment->roles()->attach($manager_role);

        $editShipment = new Permission();
		$editShipment->slug = 'edit-shipment';
		$editShipment->name = 'Edit Shipment';
		$editShipment->save();
		$editShipment->roles()->attach($admin_role);
		$editShipment->roles()->attach($manager_role);

        $deleteShipment = new Permission();
		$deleteShipment->slug = 'delete-shipment';
		$deleteShipment->name = 'Delete Shipment';
		$deleteShipment->save();
		$deleteShipment->roles()->attach($admin_role);

		$admin_role = Role::where('slug','admin')->first();
		$supervisor_role = Role::where('slug', 'supervisor')->first();
		$clerk_role = Role::where('slug', 'clerk')->first();
		$manager_role = Role::where('slug', 'manager')->first();

		$admin = new User();
		$admin->name = 'Admin';
		$admin->email = 'admin@email.com';
		$admin->password = bcrypt('testtest');
		$admin->user_verified = true;
		$admin->markEmailAsVerified();
		$admin->save();
		$admin->roles()->attach($admin_role);
		for($i=0; $i<9 ;$i++){
			$admin->permissions()->attach($permissions[$i]);
		}

		$supervisor = new User();
		$supervisor->name = 'Supervisor';
		$supervisor->email = 'supervisor@email.com';
		$supervisor->password = bcrypt('testtest');
		$supervisor->user_verified = true;
		$supervisor->markEmailAsVerified();
		$supervisor->save();
		$supervisor->roles()->attach($supervisor_role);
		$supervisor->permissions()->attach($permissions[4]);

		$clerk = new User();
		$clerk->name = 'Clerk';
		$clerk->email = 'clerk@email.com';
		$clerk->password = bcrypt('testtest');
		$clerk->user_verified = true;
		$clerk->markEmailAsVerified();
		$clerk->save();
		$clerk->roles()->attach($clerk_role);
		$clerk->permissions()->attach($permissions[3]);
		$clerk->permissions()->attach($permissions[4]);

		$manager = new User();
		$manager->name = 'Manager';
		$manager->email = 'manager@email.com';
		$manager->password = bcrypt('testtest');
		$manager->user_verified = true;
		$manager->markEmailAsVerified();
		$manager->save();
		$manager->roles()->attach($manager_role);
		$manager->permissions()->attach($permissions[6]);
		$manager->permissions()->attach($permissions[7]);

    }
}
